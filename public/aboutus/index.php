<!DOCTYPE html>
<html lang="en">

<head>
    <title>About Us</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>

<body>

    <div class="contact1">
        <div class="container-contact1">
            <div class="contact1-picX js-tilt" data-tilt>
                <img width='250px' height='150px' src="/images/logo.jpg" alt="IMG">
            </div>

            <div>
                <h1>Phakom Global Investments Ltd</h1>

                <div><br /><br />
                    <h2>Company History</h2>
                    <i>
                        Phakom global investment ltd was registered in on 15th July, 2016 to carry on the business of
                        sales
                        of automobile in Nigeria. It has since then conducted business with highest professionalism and
                        integrity that increased customers patronage. Phakom has been able to spread her business from
                        the
                        south western part of Nigeria to other places through good customer service.
                        We also deal in sales of real estate for our customers.
                    </i>
                </div>

                <div><br /><br />
                    <h2>What we do:</h2>
                    <i>
                        We specialise in the sales of vehicles to meet the growing demand of our numerous customers in
                        Nigeria. Due to the fact that the taste for luxurious goods brings about increase in demand for
                        vehicles in Nigeria, Phakom Global Investment Ltd strives to meet these yearnings in fashion,
                        professionalism, satisfaction, ease and convenience as demanded by our customers.
                        We also specialise in sales of real estate. This includes landed property, buildings in Nigeria
                        for our customers looking for such property in a choice area. We do this always in tandem with
                        what our customers’ desires, wants and needs.
                    </i>
                </div>

                <div><br /><br />
                    <h2>Vision statement:</h2>
                    <i>
                        make the best product-automobile and real estate- available to our customers with ease ,
                        convenience and satisfaction.
                    </i>
                </div>
            </div>

            <!--<br/>&nbsp;<br/>&nbsp;<p align="center">13 Bode OLABODE street, Off Aboru Road, Iyana ipaja, Lagos state.</p>-->
        </div>
    </div>




    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/tilt/tilt.jquery.min.js"></script>
    <script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
    </script>

    <!--===============================================================================================-->
    <script src="js/main.js"></script>

    <?php
	if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message']))
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$subject = $_POST['subject'];
		$message = $_POST['message'];

		$to = 'davidbowo@gmail.com';
		$subject = "Simple Shipping Contact Form | ".$subject;
		$message = $message."\n Sender: $email";
		$from = 'hello@phakom.ng';
		$headers = "From:" .$from. "\r\n" . "Reply-To: $email";
		//mail($to,$subject,$message,$headers);
	 
		echo
		('
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script>
		  swal("Message Sent.", "We have received your message.", "success");
		</script>
		');
	}
?>

</body>

</html>