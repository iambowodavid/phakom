
@php $images = App\Http\Controllers\Admin\ManageUploads::getFilesURLs($car->photos_id); @endphp

@if(isset($_GET['request_slide']))
<!DOCTYPE html>
<html lang="en">

<head>
    <script>
        var SITE_URL = 'https://wowslider.com/';
    </script>
    <script type="text/javascript" src="https://wowslider.com/images/demo/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="https://wowslider.com/sliders/demo-88/engine1/style.css" />
</head>

<body>

    <!-- Start WOWSlider.com BODY section -->
    <div id="wowslider-container1">
        <div class="ws_images">
            <ul>
				@foreach($images as $source)
                <li><img src="{{$source}}" alt="slideshow" title="{{$car->title}}" id="wows1_0" />{{$car->title}}</li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="ws_shadow"></div>

    <script type="text/javascript" src="https://wowslider.com/images/demo/wowslider.js"></script>
    <script>
        jQuery('#wowslider-container1').wowSlider({
            effect: "cube_over",
            prev: "",
            next: "",
            duration: 2 * 1000,
            delay: 5 * 1000,
            width: 960,
            height: 500,
            autoPlay: true,
            autoPlayVideo: false,
            playPause: false,
            stopOnHover: false,
            loop: false,
            bullets: 1,
            caption: true,
            captionEffect: "parallax",
            controls: true,
            responsive: 1,
            fullScreen: false,
            gestures: 2,
            onBeforeStep: 0,
            images: 0,
            staticColor: true
        });
    </script>
    <!-- End WOWSlider.com BODY section -->

</body>

</html>

@else
@php \App\Http\Controllers\ShowItem::checkViews('car', $car); @endphp

<html lang="en-US" class="wf-ralewaysansserif-n7-active wf-ralewaysansserif-n4-active wf-ubuntu-n7-active wf-ubuntu-n4-active wf-active"><!--<![endif]-->
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    @php if(count($images)>0){@endphp
	<meta property="og:image" content="{{$images[0]}}" />
	<meta property="og:url" content="{{url()->current()}}" />
	<meta property="og:image:width" content="{{getimagesize($images[0])[0]}}" />
	<meta property="og:image:height" content="{{getimagesize($images[0])[1]}}" />
	@php }@endphp

	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	
				<!--[if lt IE 9]>
	<script src="httpx://phakom.ng/carsales/wp-content/themes/classiera/js/html5.js"></script>
	<![endif]-->
		<title>{{$car->title}} | Phakom.ng</title>
						<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js" type="text/javascript" async=""></script><script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js" type="text/javascript" async=""></script><script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js" type="text/javascript" async=""></script>
						<script src="/n-design_files/webfont.js" type="text/javascript" async=""></script><script>
                            /* You can add more configuration options to webfontloader by previously defining the WebFontConfig with your options */
                            if ( typeof WebFontConfig === "undefined" ) {
                                WebFontConfig = new Object();
                            }
                            WebFontConfig['google'] = {families: ['Raleway,sans-serif:700,400', 'ubuntu:700', 'Ubuntu']};

                            (function() {
                                var wf = document.createElement( 'script' );
                                wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js';
                                wf.type = 'text/javascript';
                                wf.async = 'true';
                                var s = document.getElementsByTagName( 'script' )[0];
                                s.parentNode.insertBefore( wf, s );
                            })();
                        </script>
                        <link rel="dns-prefetch" href="https://maps.googleapis.com/">
<link rel="dns-prefetch" href="https://fonts.googleapis.com/">
<link rel="dns-prefetch" href="https://s.w.org/">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" id="layerslider-css" href="/n-design_files/layerslider.css" type="text/css" media="all">
<link rel="stylesheet" id="apsl-frontend-css-css" href="/n-design_files/frontend.css" type="text/css" media="all">
<link rel="stylesheet" id="apss-font-opensans-css" href="/n-design_files/css" type="text/css" media="all">
<link rel="stylesheet" id="apss-frontend-css-css" href="/n-design_files/frontend(1).css" type="text/css" media="all">
<link rel="stylesheet" id="woocommerce-layout-css" href="/n-design_files/woocommerce-layout.css" type="text/css" media="all">
<link rel="stylesheet" id="woocommerce-smallscreen-css" href="/n-design_files/woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)">
<link rel="stylesheet" id="woocommerce-general-css" href="/n-design_files/woocommerce.css" type="text/css" media="all">
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel="stylesheet" id="imgareaselect-css" href="/n-design_files/imgareaselect.css" type="text/css" media="all">
<link rel="stylesheet" id="watermark-style-css" href="/n-design_files/image-watermark.css" type="text/css" media="all">
<link rel="stylesheet" id="select2.min-css" href="/n-design_files/select2.min.css" type="text/css" media="all">
<link rel="stylesheet" id="jquery-ui-css" href="/n-design_files/jquery-ui.min.css" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-css" href="/n-design_files/bootstrap.css" type="text/css" media="all">
<link rel="stylesheet" id="animate.min-css" href="/n-design_files/animate.min.css" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-dropdownhover.min-css" href="/n-design_files/bootstrap-dropdownhover.min.css" type="text/css" media="all">
<link rel="stylesheet" id="classiera-components-css" href="/n-design_files/classiera-components.css" type="text/css" media="all">
<link rel="stylesheet" id="classiera-css" href="/n-design_files/classiera.css" type="text/css" media="all">
<link rel="stylesheet" id="material-design-iconic-font-css" href="/n-design_files/material-design-iconic-font.css" type="text/css" media="all">
<link rel="stylesheet" id="owl.carousel.min-css" href="/n-design_files/owl.carousel.min.css" type="text/css" media="all">
<link rel="stylesheet" id="owl.theme.default.min-css" href="/n-design_files/owl.theme.default.min.css" type="text/css" media="all">
<link rel="stylesheet" id="responsive-css" href="/n-design_files/responsive.css" type="text/css" media="all">
<link rel="stylesheet" id="classiera-map-css" href="/n-design_files/classiera-map.css" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-slider-css" href="/n-design_files/bootstrap-slider.css" type="text/css" media="all">
<link rel="stylesheet" id="newsletter-css" href="/n-design_files/style.css" type="text/css" media="all">
<link rel="stylesheet" href="/n-design_files/css(1)"><link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway,sans-serif:700,400%7Cubuntu:700%7CUbuntu"><link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway,sans-serif:700,400%7Cubuntu:700%7CUbuntu">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway,sans-serif:700,400%7Cubuntu:700%7CUbuntu"><script type="text/javascript">
/* <![CDATA[ */
var LS_Meta = {"v":"6.7.0"};
/* ]]> */
</script>
<script type="text/javascript" src="/n-design_files/greensock.js"></script>
<script src="/theme-sublime/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/n-design_files/jquery-migrate.min.js"></script>
<script type="text/javascript" src="/n-design_files/layerslider.kreaturamedia.jquery.js"></script>
<script type="text/javascript" src="/n-design_files/layerslider.transitions.js"></script>
<script type="text/javascript" src="/n-design_files/frontend.js"></script>

<meta name="generator" content="Powered by LayerSlider 6.7.0 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress.">
<!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
<meta name="generator" content="WordPress 4.9.8">
<meta name="generator" content="WooCommerce 3.4.3">
	<script type="text/javascript">
	var ajaxurl = 'httpx://phakom.ng/carsales/wp-admin/admin-ajax.php';
	</script>

	<script type="text/javascript">
	var ajaxLocation = 'httpx://phakom.ng/carsales/wp-content/themes/classiera/inc/getlocation.php';
	var classieraCurrentUserID = '0';
	</script>
	
<style type="text/css">.topBar .login-info a.register, .search-section .search-form.search-form-v1 .form-group button:hover, .search-section.search-section-v3, section.search-section-v2, .search-section.search-section-v5 .form-group button:hover, .search-section.search-section-v6 .form-v6-bg .form-group button, .category-slider-small-box ul li a:hover, .classiera-premium-ads-v3 .premium-carousel-v3 .item figure figcaption .price span:first-of-type, .classiera-box-div-v3 figure figcaption .price span:first-of-type, .classiera-box-div-v5 figure .premium-img .price, .classiera-box-div-v6 figure .premium-img .price.btn-primary.active, .classiera-box-div-v7 figure figcaption .caption-tags .price, .classiera-box-div-v7 figure:hover figcaption, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v4 figure .detail .box-icon a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v5 figure .detail .price, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure .detail .price.btn-primary.active, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure .detail .price.btn-primary.active, .advertisement-v1 .tab-divs .view-as a:hover, .advertisement-v2 .view-as .btn-group a.active, .advertisement-v2 .nav-tabs > li:active > a, .advertisement-v2 .nav-tabs > li.active > a, .advertisement-v2 .nav-tabs > li.active > a:hover, .advertisement-v2 .nav-tabs > li > a:hover, .advertisement-v2 .nav-tabs > li > a:focus, .advertisement-v2 .nav-tabs > li > a:active, .advertisement-v4 .view-head .tab-button .nav-tabs > li > a:hover, .advertisement-v4 .view-head .tab-button .nav-tabs > li > a:active, .advertisement-v4 .view-head .tab-button .nav-tabs > li > a:focus, .advertisement-v4 .view-head .tab-button .nav-tabs > li:hover:before, .advertisement-v4 .view-head .tab-button .nav-tabs > li.active:before, .advertisement-v4 .view-head .tab-button .nav-tabs > li.active > a, .members .members-text h3, .members-v2 .members-text h4, .members-v4.members-v5 .member-content a.btn:hover, .locations .location-content .location .location-icon, .locations .location-content .location .location-icon .tip:after, .locations .location-content-v6 figure.location figcaption .location-caption span, .pricing-plan .pricing-plan-content .pricing-plan-box .pricing-plan-price, .pricing-plan-v2 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-price, .pricing-plan-v3 .pricing-plan-content .pricing-plan-box .pricing-plan-heading h4 span, .pricing-plan-v4 .pricing-plan-content .pricing-plan-box.popular, .pricing-plan-v4 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-heading, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn:hover, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn:focus, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular, .pricing-plan-v6.pricing-plan-v7, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn:hover, footer .widget-box .widget-content .footer-pr-widget-v1:hover .media-body .price, footer .widget-box .widget-content .grid-view-pr li span .hover-posts span, footer .widget-box .tagcloud a:hover, .footer-bottom ul.footer-bottom-social-icon li a:hover, #back-to-top:hover, .sidebar .widget-box .widget-content .grid-view-pr li span .hover-posts span, .sidebar .widget-box .tagcloud a:hover, .sidebar .widget-box .user-make-offer-message .nav > li > a:hover, .sidebar .widget-box .user-make-offer-message .nav > li.btnWatch button:hover, .sidebar .widget-box .user-make-offer-message .nav > li.active > a, .sidebar .widget-box .user-make-offer-message .nav > li.active > button, .inner-page-content .classiera-advertisement .item.item-list .classiera-box-div figure figcaption .price.visible-xs, .author-box .author-social .author-social-icons li > a:hover, .user-pages aside .user-page-list li a:hover, .user-pages aside .user-page-list li.active a, .user-pages aside .user-submit-ad .btn-user-submit-ad:hover, .user-pages .user-detail-section .user-social-profile-links ul li a:hover, .user-pages .user-detail-section .user-ads.follower .media .media-body > .classiera_follow_user input[type='submit']:hover, .user-pages .user-detail-section .user-ads.follower .media .media-body > .classiera_follow_user input[type='submit']:focus, .submit-post form .classiera-post-main-cat ul li a:hover, .submit-post form .classiera-post-main-cat ul li a:focus, .submit-post form .classiera-post-main-cat ul li.active a, .classiera_follow_user > input[type='submit']:hover, .classiera_follow_user > input[type='submit']:focus, .mobile-app-button li a:hover, .mobile-app-button li a:focus, .related-blog-post-section .navText a:hover, .pagination > li > a:hover, .pagination > li span:hover, .pagination > li:first-child > a:hover, .pagination > li:first-child span:hover, .pagination > li:last-child > a:hover, .pagination > li:last-child span:hover, .inputfile-1:focus + label, .inputfile-1.has-focus + label, .inputfile-1 + label:hover, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown .category-menu-btn span, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown.open .category-menu-btn, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > li > .dropdown-menu > li > a:hover, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav .dropdown-menu li > a:hover, .classiera-navbar.classiera-navbar-v6 .navbar-default .navbar-nav > li > a:hover:after, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:last-of-type:hover, .classiera-navbar.classiera-navbar-v6 .dropdown .dropdown-menu, .offcanvas-light .log-reg-btn .offcanvas-log-reg-btn:hover, .offcanvas-light.offcanvas-dark .log-reg-btn .offcanvas-log-reg-btn:hover, .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary, .btn-primary.active:hover, .btn-primary:active:hover, .btn-primary:active, .btn-primary.active, .btn-primary.outline:hover, .btn-primary.outline:focus, .btn-primary.outline:active, .btn-primary.outline.active, .open > .dropdown-toggle.btn-primary, .btn-primary.outline:active, .btn-primary.outline.active, .btn-primary.raised:active, .btn-primary.raised.active, .btn-style-four.active, .btn-style-four:hover, .btn-style-four:focus, .btn-style-four:active, .social-icon:hover, .social-icon-v2:hover, .woocommerce .button:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, #ad-address span:hover i, .search-section.search-section-v3, .search-section.search-section-v4, #showNum:hover, .price.btn.btn-primary.round.btn-style-six.active, .woocommerce ul.products > li.product a > span, .woocommerce div.product .great, span.ad_type_display{ background: #9b0103 !important; } .topBar .contact-info span i, .search-section.search-section-v5 .form-group button, .category-slider-small-box.outline-box ul li a:hover, .section-heading-v1.section-heading-with-icon h3 i, .classiera-premium-ads-v3 .premium-carousel-v3 .item figure figcaption h5 a:hover, .classiera-premium-ads-v3 .premium-carousel-v3 .item figure figcaption p a:hover, .classiera-box-div-v2 figure figcaption h5 a:hover, .classiera-box-div-v2 figure figcaption p span, .classiera-box-div-v3 figure figcaption h5 a:hover, .classiera-box-div-v3 figure figcaption span.category a:hover, .classiera-box-div-v4 figure figcaption h5 a:hover, .classiera-box-div-v5 figure figcaption h5 a:hover, .classiera-box-div-v5 figure figcaption .category span a:hover, .classiera-box-div-v6 figure figcaption .content > a:hover, .classiera-box-div-v6 figure figcaption .content h5 a:hover, .classiera-box-div-v6 figure figcaption .content .category span, .classiera-box-div-v6 figure .box-div-heading .category span, .classiera-category-ads-v4 .category-box .category-box-over .category-box-content h3 a:hover, .category-v2 .category-box .category-content .view-button a:hover, .category-v3 .category-content h4 a:hover, .category-v3 .category-content .view-all:hover, .category-v3 .category-content .view-all:hover i, .category-v5 .categories li .category-content h4 a:hover, .category-v7 .category-box figure figcaption ul li a:hover, .category-v7 .category-box figure figcaption > a:hover, .category-v7 .category-box figure figcaption > a:hover i, .category-v7 .category-box figure figcaption ul li a:hover i, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v3 figure figcaption .post-tags span i, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v3 figure figcaption .post-tags a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v5 figure .detail .box-icon a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure figcaption .content h5 a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure .detail .box-icon a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure figcaption .content h5 a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure .detail .box-icon a:hover, .advertisement-v1 .tab-divs .view-as a.active, .advertisement-v1 .tab-divs .view-as a.active i, .advertisement-v3 .view-head .tab-button .nav-tabs > li > a:hover, .advertisement-v3 .view-head .tab-button .nav-tabs > li > a:active, .advertisement-v3 .view-head .tab-button .nav-tabs > li > a:focus, .advertisement-v3 .view-head .tab-button .nav-tabs > li.active > a, .advertisement-v3 .view-head .view-as a:hover i, .advertisement-v3 .view-head .view-as a.active i, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a:hover, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a:active, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a:focus, .advertisement-v6 .view-head .tab-button .nav-tabs > li.active > a, .advertisement-v6 .view-head .view-as a:hover, .advertisement-v6 .view-head .view-as a.active, .members-v4.members-v5 .member-content h1, .members-v4.members-v5 .member-content h4, .members-v4.members-v5 .member-content a.btn, .locations .location-content-v2 .location h5 a:hover, .locations .location-content-v3 .location .location-content h5 a:hover, .locations .location-content-v5 ul li .location-content h5 a:hover, .locations .location-content-v6 figure.location figcaption .location-caption > a, .pricing-plan-v4 .pricing-plan-content .pricing-plan-box .pricing-plan-heading .price-title, .pricing-plan-v5 .pricing-plan-content .pricing-plan-box .pricing-plan-text ul li i, .pricing-plan-v5 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-button h3, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-button .btn:hover, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-heading h2, footer .widget-box .widget-content .footer-pr-widget-v1 .media-body h4 a:hover, footer .widget-box .widget-content .footer-pr-widget-v1 .media-body span.category a:hover, footer .widget-box .widget-content .footer-pr-widget-v2 .media-body h5 a:hover, footer .widget-box .widget-content ul li h5 a:hover, footer .widget-box .widget-content ul li p span a:hover, footer .widget-box .widget-content .category > li > a:hover, footer .widget-box > ul > li a:hover, footer .widget-box > ul > li a:focus, footer .widgetContent .cats ul > li a:hover, footer footer .widgetContent .cats > ul > li a:focus, .blog-post-section .blog-post .blog-post-content h4 a:hover, .blog-post-section .blog-post .blog-post-content p span a:hover, .sidebar .widget-box .widget-title h4 i, .sidebar .widget-box .widget-content .footer-pr-widget-v1 .media-body h4 a:hover, .sidebar .widget-box .widget-content .footer-pr-widget-v1 .media-body .category a:hover, .sidebar .widget-box .widget-content .footer-pr-widget-v2 .media-body h5 a:hover, .sidebar .widget-box .widget-content ul li h5 a:hover, .sidebar .widget-box .widget-content ul li p span a:hover, .sidebar .widget-box .widget-content ul li > a:hover, .sidebar .widget-box .user-make-offer-message .nav > li > a, .sidebar .widget-box .user-make-offer-message .nav > li .browse-favourite a, .sidebar .widget-box .user-make-offer-message .nav > li.btnWatch button, .sidebar .widget-box .user-make-offer-message .nav > li > a i, .sidebar .widget-box .user-make-offer-message .nav > li.btnWatch button i, .sidebar .widget-box .user-make-offer-message .nav > li .browse-favourite a i, .sidebar .widget-box > ul > li > a:hover, .sidebar .widget-box > ul > li > a:focus, .sidebar .widgetBox .widgetContent .cats ul > li > a:hover, .sidebar .widget-box .widgetContent .cats ul > li > a:focus, .sidebar .widget-box .menu-all-pages-container ul li a:hover, .sidebar .widget-box .menu-all-pages-container ul li a:focus, .inner-page-content .breadcrumb > li a:hover, .inner-page-content .breadcrumb > li a:hover i, .inner-page-content .breadcrumb > li.active, .inner-page-content article.article-content.blog h3 a:hover, .inner-page-content article.article-content.blog p span a:hover, .inner-page-content article.article-content.blog .tags a:hover, .inner-page-content article.article-content blockquote:before, .inner-page-content article.article-content ul li:before, .inner-page-content article.article-content ol li a, .inner-page-content .login-register.login-register-v1 form .form-group p a:hover, .author-box .author-contact-details .contact-detail-row .contact-detail-col span a:hover, .author-info .media-heading a:hover, .author-info span i, .user-pages .user-detail-section .user-contact-details ul li a:hover, .user-pages .user-detail-section .user-ads .media .media-body .media-heading a:hover, .user-pages .user-detail-section .user-ads .media .media-body p span a:hover, .user-pages .user-detail-section .user-ads .media .media-body p span.published i, .user-pages .user-detail-section .user-packages .table tr td.text-success, form .search-form .search-form-main-heading a i, form .search-form #innerSearch .inner-search-box .inner-search-heading i, .submit-post form .form-main-section .classiera-dropzone-heading i, .submit-post form .form-main-section .iframe .iframe-heading i, .single-post-page .single-post .single-post-title .post-category span a:hover, .single-post-page .single-post .description p a, .single-post-page .single-post > .author-info a:hover, .single-post-page .single-post > .author-info .contact-details .fa-ul li a:hover, .classiera_follow_user > input[type='submit'], .single-post .description ul li:before, .single-post .description ol li a, .mobile-app-button li a i, #wp-calendar td#today, td#prev a:hover, td#next a:hover, td#prev a:focus, td#next a:focus, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown .category-menu-btn:hover span i, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown.open .category-menu-btn span i, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown .dropdown-menu li a:hover, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > li > a:hover, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > .active > a, .classiera-navbar.classiera-navbar-v4 .dropdown-menu > li > a:hover, .classiera-navbar.classiera-navbar-v4 .dropdown-menu > li > a:hover i, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .menu-btn i, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav li.active > a, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav li > a:hover, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .login-reg .lr-with-icon:hover, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type i, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type:hover, .offcanvas-light .navmenu-brand .offcanvas-button i, .offcanvas-light .nav > li > a:hover, .offcanvas-light .nav > li > a:focus, .offcanvas-light .navmenu-nav > .open > a, .offcanvas-light .navmenu-nav .open .dropdown-menu > li > a:hover, .offcanvas-light .navmenu-nav .open .dropdown-menu > li > a:focus, .offcanvas-light .navmenu-nav .open .dropdown-menu > li > a:active, .btn-primary.btn-style-six:hover, .btn-primary.btn-style-six.active, input[type=radio]:checked + label:before, input[type='checkbox']:checked + label:before, .woocommerce-info::before, .woocommerce .woocommerce-info a:hover, .woocommerce .woocommerce-info a:focus, #ad-address span a:hover, #ad-address span a:focus, #getLocation:hover i, #getLocation:focus i, .offcanvas-light .nav > li.active > a, .classiera-box-div-v4 figure figcaption h5 a:hover, .classiera-box-div-v4 figure figcaption h5 a:focus, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular h1, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-button .btn.round:hover, .color, .classiera-box-div.classiera-box-div-v7 .buy-sale-tag, .offcanvas-light .nav > li.dropdown ul.dropdown-menu li.active > a, .classiera-navbar.classiera-navbar-v4 ul.nav li.dropdown ul.dropdown-menu > li.active > a, .classiera-navbar-v6 .offcanvas-light ul.nav li.dropdown ul.dropdown-menu > li.active > a, .sidebar .widget-box .author-info a:hover, .submit-post form .classiera-post-sub-cat ul li a:focus, .submit-post form .classiera_third_level_cat ul li a:focus, .woocommerce div.product p.price ins, p.classiera_map_div__price span, .author-info .media-heading i{ color: #9b0103 !important; } .pricing-plan-v2 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-heading{ background:rgba( 155,1,3,.75 )} .pricing-plan-v2 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-heading::after{ border-top-color:rgba( 155,1,3,.75 )} footer .widget-box .widget-content .grid-view-pr li span .hover-posts{ background:rgba( 155,1,3,.5 )} .advertisement-v1 .tab-button .nav-tabs > li.active > a, .advertisement-v1 .tab-button .nav-tabs > li.active > a:hover, .advertisement-v1 .tab-button .nav-tabs > li.active > a:focus, .advertisement-v1 .tab-button .nav > li > a:hover, .advertisement-v1 .tab-button .nav > li > a:focus, .members-v4, form .search-form #innerSearch .inner-search-box .slider-handle, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type:hover i{ background-color: #9b0103 !important; } .search-section .search-form.search-form-v1 .form-group button:hover, .search-section.search-section-v5 .form-group button, .search-section.search-section-v5 .form-group button:hover, .search-section.search-section-v6 .form-v6-bg .form-group button, .advertisement-v1 .tab-button .nav-tabs > li.active > a, .advertisement-v1 .tab-button .nav-tabs > li.active > a:hover, .advertisement-v1 .tab-button .nav-tabs > li.active > a:focus, .advertisement-v1 .tab-button .nav > li > a:hover, .advertisement-v1 .tab-button .nav > li > a:focus, .advertisement-v1 .tab-divs .view-as a:hover, .advertisement-v1 .tab-divs .view-as a.active, .advertisement-v4 .view-head .tab-button .nav-tabs > li > a:hover, .advertisement-v4 .view-head .tab-button .nav-tabs > li > a:active, .advertisement-v4 .view-head .tab-button .nav-tabs > li > a:focus, .advertisement-v4 .view-head .tab-button .nav-tabs > li.active > a, .members-v3 .members-text .btn.outline:hover, .members-v4.members-v5 .member-content ul li span, .members-v4.members-v5 .member-content a.btn, .members-v4.members-v5 .member-content a.btn:hover, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn:hover, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn:focus, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-heading, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-text, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn:hover, .sidebar .widget-box .user-make-offer-message .nav > li > a, .sidebar .widget-box .user-make-offer-message .nav > li .browse-favourite a, .sidebar .widget-box .user-make-offer-message .nav > li.btnWatch button, .user-pages aside .user-submit-ad .btn-user-submit-ad:hover, .user-pages .user-detail-section .user-ads.follower .media .media-body > .classiera_follow_user input[type='submit']:hover, .user-pages .user-detail-section .user-ads.follower .media .media-body > .classiera_follow_user input[type='submit']:focus, .submit-post form .form-main-section .active-post-type .post-type-box, .submit-post form .classiera-post-main-cat ul li a:hover, .submit-post form .classiera-post-main-cat ul li a:focus, .submit-post form .classiera-post-main-cat ul li.active a, .classiera-upload-box.classiera_featured_box, .classiera_follow_user > input[type='submit'], .related-blog-post-section .navText a:hover, .pagination > li > a:hover, .pagination > li span:hover, .pagination > li:first-child > a:hover, .pagination > li:first-child span:hover, .pagination > li:last-child > a:hover, .pagination > li:last-child span:hover, .classiera-navbar.classiera-navbar-v1 .betube-search .btn.outline:hover, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type i, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type:hover i, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:last-of-type, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:last-of-type:hover, .classiera-navbar.classiera-navbar-v6 .dropdown .dropdown-menu, .offcanvas-light .navmenu-brand .offcanvas-button, .offcanvas-light .log-reg-btn .offcanvas-log-reg-btn:hover, .btn-primary.outline:hover, .btn-primary.outline:focus, .btn-primary.outline:active, .btn-primary.outline.active, .open > .dropdown-toggle.btn-primary, .btn-primary.outline:active, .btn-primary.outline.active, .btn-style-four.active, .btn-style-four.active:hover, .btn-style-four.active:focus, .btn-style-four.active:active, .btn-style-four:hover, .btn-style-four:focus, .btn-style-four:active, #showNum:hover, .user_inbox_content > .tab-content .tab-pane .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border-color:#9b0103 !important; } .advertisement-v4 .view-head .tab-button .nav-tabs > li > a span.arrow-down, .advertisement-v4 .view-head .tab-button .nav-tabs > li:hover:after, .advertisement-v4 .view-head .tab-button .nav-tabs > li.active:after, .locations .location-content .location .location-icon .tip, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown .dropdown-menu, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > li > .dropdown-menu, .classiera-navbar.classiera-navbar-v4 .dropdown-menu, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav .dropdown-menu, .woocommerce-error, .woocommerce-info, .woocommerce-message{ border-top-color:#9b0103; } .locations .location-content-v2 .location:hover, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown .dropdown-menu:before, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > li > a:hover, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > li > .dropdown-menu:before, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > .active > a{ border-bottom-color:#9b0103 !important; } { box-shadow:0 3px 0 0 #9b0103 !important; } .pagination > li.active a, .pagination > li.disabled a, .pagination > li.active a:focus, .pagination > li.active a:hover, .pagination > li.disabled a:focus, .pagination > li.disabled a:hover, .pagination > li:first-child > a, .pagination > li:first-child span, .pagination > li:last-child > a, .pagination > li:last-child span, .classiera-navbar.classiera-navbar-v3.affix, .classiera-navbar.classiera-navbar-v3 .navbar-nav > li > .dropdown-menu li a:hover, .classiera-navbar.classiera-navbar-v4 .dropdown-menu > li > a:hover, .classiera-navbar.classiera-navbar-v6 .dropdown .dropdown-menu > li > a:hover, .classiera-navbar.classiera-navbar-v6 .dropdown .dropdown-menu > li > a:focus, .btn-primary, .btn-primary.btn-style-five:hover, .btn-primary.btn-style-five.active, .btn-primary.btn-style-six:hover, .btn-primary.btn-style-six.active, .input-group-addon, .woocommerce .button, .woocommerce a.button, .woocommerce .button.alt, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, #ad-address span i, .search-section .search-form .form-group .help-block, .search-section .search-form.search-form-v1 .form-group button, .search-section.search-section-v2 .form-group button, .search-section.search-section-v4 .search-form .btn:hover, .category-slider-small-box ul li a, .category-slider-small-box.outline-box ul li a:hover, .classiera-premium-ads-v3 .premium-carousel-v3 .owl-dots .owl-dot.active span, .classiera-premium-ads-v3 .premium-carousel-v3 .owl-dots .owl-dot:hover span, .classiera-box-div-v7 figure:hover:after, .category-v2 .category-box .category-content ul li a:hover i, .category-v6 .category-box figure .category-box-hover > span, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v3 figure figcaption .price span:last-of-type, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v5 figure .detail .box-icon a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure .detail .box-icon a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure .detail .box-icon a:hover, .advertisement-v1 .tab-button .nav-tabs > li > a, .advertisement-v5 .view-head .tab-button .nav-tabs > li > a:hover, .advertisement-v5 .view-head .tab-button .nav-tabs > li > a:active, .advertisement-v5 .view-head .tab-button .nav-tabs > li > a:focus, .advertisement-v5 .view-head .tab-button .nav-tabs > li.active > a, .advertisement-v5 .view-head .view-as a:hover, .advertisement-v5 .view-head .view-as a.active, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a:hover, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a:active, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a:focus, .advertisement-v6 .view-head .tab-button .nav-tabs > li.active > a, .advertisement-v6 .view-head .view-as a:hover, .advertisement-v6 .view-head .view-as a.active, .locations .location-content .location:hover, .call-to-action .call-to-action-box .action-box-heading .heading-content i, .pricing-plan-v2 .pricing-plan-content .pricing-plan-box .pricing-plan-price, .pricing-plan-v5 .pricing-plan-content .pricing-plan-box .pricing-plan-heading, .pricing-plan-v6, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-button .btn:hover, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular, .partners-v3 .partner-carousel-v3 .owl-dots .owl-dot.active span, .partners-v3 .partner-carousel-v3 .owl-dots .owl-dot:hover span, #back-to-top, .custom-wp-search .btn-wp-search, .user-pages .user-detail-section .user-ads.follower .media .media-body > .classiera_follow_user input[type='submit'], .single-post-page .single-post #single-post-carousel .single-post-carousel-controls .carousel-control span, .section-bg-black, #ad-address span i, .classiera-navbar.classiera-navbar-v4 ul.nav li.dropdown ul.dropdown-menu > li.active > a, .classiera-navbar.classiera-navbar-v6 ul.nav li.dropdown ul.dropdown-menu > li.active > a, #showNum{ background: #232323; } .members-v4.members-v5, .classiera-navbar.classiera-navbar-v6{ background-color: #232323 !important; } h1 > a, h2 > a, h3 > a, h4 > a, h5 > a, h6 > a,.classiera-navbar.classiera-navbar-v1 .navbar-default .navbar-nav > li > a, .classiera-navbar.classiera-navbar-v1 .navbar-default .navbar-nav > .active > a, .classiera-navbar.classiera-navbar-v1 .navbar-default .navbar-nav > .active > a:hover, .classiera-navbar.classiera-navbar-v1 .navbar-default .navbar-nav > .active > a:focus, .classiera-navbar.classiera-navbar-v1 .dropdown-menu > li > a:hover, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown .category-menu-btn, .classiera-navbar.classiera-navbar-v2 .category-menu-dropdown .dropdown-menu li a, .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > li > .dropdown-menu > li > a, .classiera-navbar.classiera-navbar-v4 .navbar-nav > li > a:hover, .classiera-navbar.classiera-navbar-v4 .navbar-nav > li > a:focus, .classiera-navbar.classiera-navbar-v4 .navbar-nav > li > a:link, .classiera-navbar.classiera-navbar-v4 .navbar-nav > .active > a, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav li > a, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav .dropdown-menu li > a, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .login-reg .lr-with-icon, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type:hover i, .classiera-navbar.classiera-navbar-v6 .dropdown .dropdown-menu > li > a, .classiera-navbar.classiera-navbar-v6 .dropdown .dropdown-menu > li > a i, .btn-primary.outline, .radio label a, .checkbox label a, #getLocation, .search-section.search-section-v6 .form-v6-bg .form-group button, .category-slider-small-box ul li a:hover, .category-slider-small-box.outline-box ul li a, .classiera-static-slider-v2 .classiera-static-slider-content h1, .classiera-static-slider-v2 .classiera-static-slider-content h2, .classiera-static-slider-v2 .classiera-static-slider-content h2 span, .section-heading-v5 h1, .section-heading-v6 h1, .classiera-premium-ads-v3 .premium-carousel-v3 .item figure figcaption .price, .classiera-premium-ads-v3 .premium-carousel-v3 .item figure figcaption .price span:last-of-type, .classiera-premium-ads-v3 .premium-carousel-v3 .item figure figcaption h5 a, .classiera-premium-ads-v3 .navText a i, .classiera-premium-ads-v3 .navText span, .classiera-box-div-v1 figure figcaption h5 a, .classiera-box-div-v1 figure figcaption p a:hover, .classiera-box-div-v2 figure figcaption h5 a, .classiera-box-div-v3 figure figcaption .price, .classiera-box-div-v3 figure figcaption .price span:last-of-type, .classiera-box-div-v3 figure figcaption h5 a, .classiera-box-div-v4 figure figcaption h5 a, .classiera-box-div-v5 figure figcaption h5 a, .classiera-box-div-v6 figure .premium-img .price.btn-primary.active, .classiera-box-div-v7 figure figcaption .caption-tags .price, .classiera-box-div-v7 figure figcaption .content h5 a, .classiera-box-div-v7 figure figcaption .content > a, .classiera-box-div-v7 figure:hover figcaption .content .category span, .classiera-box-div-v7 figure:hover figcaption .content .category span a, .category-v1 .category-box .category-content ul li a:hover, .category-v2 .category-box .category-content .view-button a, .category-v3 .category-content h4 a, .category-v3 .category-content .view-all, menu-category .navbar-header .navbar-brand, .menu-category .navbar-nav > li > a:hover, .menu-category .navbar-nav > li > a:active, .menu-category .navbar-nav > li > a:focus, .menu-category .dropdown-menu li a:hover, .category-v5 .categories li, .category-v5 .categories li .category-content h4 a, .category-v6 .category-box figure figcaption > span i, .category-v6 .category-box figure .category-box-hover h3 a, .category-v6 .category-box figure .category-box-hover p, .category-v6 .category-box figure .category-box-hover ul li a, .category-v6 .category-box figure .category-box-hover > a, .category-v7 .category-box figure .cat-img .cat-icon i, .category-v7 .category-box figure figcaption h4 a, .category-v7 .category-box figure figcaption > a, .classiera-advertisement .item.item-list .classiera-box-div figure figcaption .post-tags span, .classiera-advertisement .item.item-list .classiera-box-div figure figcaption .post-tags a:hover, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v5 figure .detail .box-icon a, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure figcaption .content h5 a, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure .detail .price.btn-primary.active, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure .detail .box-icon a, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure figcaption .content h5 a, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure .detail .price.btn-primary.active, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure .detail .box-icon a, .advertisement-v4 .view-head .tab-button .nav-tabs > li > span, .advertisement-v5 .view-head .tab-button .nav-tabs > li > a, .advertisement-v5 .view-head .view-as a, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a, .advertisement-v6 .view-head .view-as a, .members-v2 .members-text h1, .members-v4 .member-content p, .members-v4.members-v5 .member-content a.btn:hover, .locations .location-content .location a .loc-head, .locations .location-content-v2 .location h5 a, .locations .location-content-v3 .location .location-content h5 a, .locations .location-content-v5 ul li .location-content h5 a, .locations .location-content-v6 figure.location figcaption .location-caption span i, .pricing-plan-v4 .pricing-plan-content .pricing-plan-box.popular ul li, .pricing-plan-v5 .pricing-plan-content .pricing-plan-box .pricing-plan-button h3 small, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button h1, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn:hover, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn:focus, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn:hover, .partners-v3 .navText a i, .partners-v3 .navText span, footer .widget-box .widget-content .grid-view-pr li span .hover-posts span, .blog-post-section .blog-post .blog-post-content h4 a, .sidebar .widget-box .widget-title h4, .sidebar .widget-box .widget-content .footer-pr-widget-v1 .media-body h4 a, .sidebar .widget-box .widget-content .footer-pr-widget-v2 .media-body h5 a, .sidebar .widget-box .widget-content .grid-view-pr li span .hover-posts span, .sidebar .widget-box .widget-content ul li h5 a, .sidebar .widget-box .contact-info .contact-info-box i, .sidebar .widget-box .contact-info .contact-info-box p, .sidebar .widget-box .author-info a, .sidebar .widget-box .user-make-offer-message .tab-content form label, .sidebar .widget-box .user-make-offer-message .tab-content form .form-control-static, .inner-page-content article.article-content.blog h3 a, .inner-page-content article.article-content.blog .tags > span, .inner-page-content .login-register .social-login.social-login-or:after, .inner-page-content .login-register.login-register-v1 .single-label label, .inner-page-content .login-register.login-register-v1 form .form-group p a, .border-section .user-comments .media .media-body p + h5 a:hover, .author-box .author-desc p strong, .author-info span.offline i, .user-pages aside .user-submit-ad .btn-user-submit-ad, .user-pages .user-detail-section .about-me p strong, .user-pages .user-detail-section .user-ads .media .media-body .media-heading a, form .search-form .search-form-main-heading a, form .search-form #innerSearch .inner-search-box input[type='checkbox']:checked + label::before, form .search-form #innerSearch .inner-search-box p, .submit-post form .form-main-section .classiera-image-upload .classiera-image-box .classiera-upload-box .classiera-image-preview span i, .submit-post form .terms-use a, .submit-post.submit-post-v2 form .form-group label.control-label, .single-post-page .single-post .single-post-title > .post-price > h4, .single-post-page .single-post .single-post-title h4 a, .single-post-page .single-post .details .post-details ul li p, .single-post-page .single-post .description .tags span, .single-post-page .single-post .description .tags a:hover, .single-post-page .single-post > .author-info a, .classieraAjaxInput .classieraAjaxResult ul li a, .pricing-plan-v4 .pricing-plan-content .pricing-plan-box.popular .price-title{ color: #232323; } .pagination > li.active a, .pagination > li.disabled a, .pagination > li.active a:focus, .pagination > li.active a:hover, .pagination > li.disabled a:focus, .pagination > li.disabled a:hover, .pagination > li:first-child > a, .pagination > li:first-child span, .pagination > li:last-child > a, .pagination > li:last-child span, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .menu-btn, .btn-primary.outline, .btn-primary.btn-style-five:hover, .btn-primary.btn-style-five.active, .btn-primary.btn-style-six:hover, .btn-primary.btn-style-six.active, .input-group-addon, .search-section .search-form.search-form-v1 .form-group button, .category-slider-small-box.outline-box ul li a, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v5 figure .detail .box-icon a, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v6 figure .detail .box-icon a, .classiera-advertisement .item.item-list .classiera-box-div.classiera-box-div-v7 figure .detail .box-icon a, .advertisement-v5 .view-head .tab-button .nav-tabs > li > a, .advertisement-v5 .view-head .view-as a, .advertisement-v5 .view-head .view-as a:hover, .advertisement-v5 .view-head .view-as a.active, .advertisement-v6 .view-head .tab-button .nav-tabs > li > a, .advertisement-v6 .view-head .view-as a, .advertisement-v6 .view-head .view-as a:hover, .advertisement-v6 .view-head .view-as a.active, .locations .location-content .location:hover, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-heading, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-text, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-button .btn:hover, .user-pages .user-detail-section .user-ads.follower .media .media-body > .classiera_follow_user input[type='submit'], #showNum{ border-color: #232323; } .classiera-navbar.classiera-navbar-v1 .dropdown-menu{ border-top-color: #232323; } .search-section .search-form .form-group .help-block ul::before{ border-bottom-color: #232323; } .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav .dropdown-menu li > a:hover, .classiera-navbar.classiera-navbar-v5 .custom-menu-v5 .navbar-nav .dropdown-menu li > a:focus, .search-section.search-section-v5 .form-group .input-group-addon i, .classiera-box-div-v6 figure .premium-img .price.btn-primary.active, .classiera-box-div-v7 figure figcaption .caption-tags .price, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn:hover, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular .pricing-plan-button .btn, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type:hover i, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn.round:hover, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-content .pricing-plan-box .pricing-plan-button .btn:hover, .pricing-plan-v4 .pricing-plan-content .pricing-plan-box.popular .price-title, .members-v4.members-v5 .member-content a.btn:hover, .classiera-box-div .btn-primary.btn-style-six.active{color: #232323 !important; } .btn-primary.btn-style-six:hover, .btn-primary.btn-style-six.active, .pricing-plan-v6.pricing-plan-v7 .pricing-plan-box.popular, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-button .btn.round:hover, .classiera-navbar.classiera-navbar-v3 ul.navbar-nav li.dropdown ul.dropdown-menu > li.active > a, .search-section.search-section-v2 .form-group button:hover{background: #232323 !important; } .btn-primary.btn-style-six:hover, .pricing-plan-v6 .pricing-plan-content .pricing-plan-box.popular .pricing-plan-button .btn.round:hover{border-color: #232323 !important; } .topBar, .topBar.topBar-v3{ background: #444444; } .classiera-navbar.classiera-navbar-v2, .classiera-navbar.classiera-navbar-v2 .navbar-default, .classiera-navbar.classiera-navbar-v3, .classiera-navbar.classiera-navbar-v3.affix, .home .classiera-navbar.classiera-navbar-v6{ background: #fafafa !important; } .classiera-navbar.classiera-navbar-v2 .navbar-default .navbar-nav > li > a, .classiera-navbar.classiera-navbar-v3 .nav > li > a, .classiera-navbar.classiera-navbar-v6 .navbar-default .navbar-nav > li > a, .classiera-navbar.classiera-navbar-v6 .navbar-default .login-reg a:first-of-type{ color: #444444 !important; } footer .widget-box .tagcloud a{ background: #444444 !important; } footer .widget-box .tagcloud a{ color: #ffffff !important; } .footer-bottom{ background: #444444 !important; } .footer-bottom p, .footer-bottom p a, .footer-bottom ul.footer-bottom-social-icon span{ color: #8e8e8e !important; } .members-v1 .members-text h2.callout_title, .members-v4 .member-content h1, .members-v4 .member-content ul li{ color: #ffffff !important; } .members-v4 .member-content ul li span{border-color: #ffffff; } .members-v1 .members-text h2.callout_title_second, .members-v4 .member-content h4{ color: #ffffff !important; } .members-v1 .members-text p, .members-v4 .member-content p{ color: #ffffff !important; } 	section.classiera-static-slider, section.classiera-static-slider-v2, section.classiera-simple-bg-slider{
				background-color:#fff !important;
		background-image:url("");
		background-repeat:;
		background-position:;
		background-size:;
		background-attachment:;
			}
	section.classiera-static-slider .classiera-static-slider-content h1, section.classiera-static-slider-v2 .classiera-static-slider-content h1, section.classiera-simple-bg-slider .classiera-simple-bg-slider-content h1{
		color:#fff;
		font-size:60px;
		font-family:Raleway,sans-serif !important;
		font-weight:700;
		line-height:60px;
		text-align:;
		letter-spacing:;
	}
	section.classiera-static-slider .classiera-static-slider-content h2, section.classiera-static-slider-v2 .classiera-static-slider-content h2, section.classiera-simple-bg-slider .classiera-simple-bg-slider-content h4{
		color:#fff;
		font-size:24px;
		font-family:Raleway,sans-serif !important;
		font-weight:400;
		line-height:24px;
		text-align:;
		letter-spacing:;
	}
	</style>	
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/35/2/common.js"></script><script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/35/2/util.js"></script>
    
	
</head>
<body class="archive category category-cars category-76 woocommerce-js">
	
	<header style="padding-top: 0px;">
	
		<!-- NavBar -->
<section class="classiera-navbar  classieraNavAffix classiera-navbar-v5">
		<div class="">
				<!-- mobile off canvas nav -->
				
				<!-- mobile off canvas nav -->
		<!--Primary Nav-->
		<nav class="navbar navbar-default ">
					<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand-custom" href="/">
    <h1>Phakom.ng</h1>
									<!--<img class="img-responsive" src="/n-design_files/logouklass2.jpg" alt="Uklass.ng">-->
								</a>
			</div><!--navbar-header-->
					<!--collapse navbar-collapse visible-lg-->
		</nav>
		<!--Primary Nav-->
		</div><!--container-->
</section>
<!-- NavBar -->	<!-- Mobile App button -->
	<div class="mobile-submit affix" style="">
        <ul class="list-unstyled list-inline mobile-app-button">
		            <li>
                <a href="/login/">
                    <i class="fa fa-sign-in-alt"></i>
                    <span>Login</span>
                </a>
            </li>
            <li>
                <a href="/login/">
                    <i class="fa fa-edit"></i>
                    <span>Submit Ad</span>
                </a>
            </li>
            <li>
                <a href="/register/">
                    <i class="fa fa-registered"></i>
                    <span>Get Registered</span>
                </a>
            </li>
		         </ul>
    </div>
	<!-- Mobile App button -->

	</header>
	
	
	
	<section class="search-section search-section-v5" style="background: #9b0103;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form data-toggle="validator" role="form" class="search-form search-form-v5 form-inline" action="/cars" method="get" novalidate="true">
				<input type='hidden' name='dosearch' value='true'/>
					<!--Select Category-->					
					<div class="form-group clearfix">
						<div class="input-group side-by-side-input inner-addon right-addon pull-left flip">
							<i class="form-icon form-icon-size-small fa fa-sort"></i>							
							<select class="form-control form-control-sm" name="category" id="ajaxSelectCat">
								<option selected="" disabled="">All Categories</option>
																	<option value="cars">Cars</option>	
																			<option value="motorcycles">Motorcycles</option>	
																			<option value="trucks">Trucks</option>	
																			<option value="buses">Buses</option>	
																			<option value="agricultural_vehicles">Agricultural Vehicles</option>	
																			<option value="">Uncategorized</option>	
																	</select>
						</div>
						<div class="side-by-side-input pull-left flip classieraAjaxInput">
							<input type="text" name="keyword" id="classieraSearchAJax" class="form-control form-control-sm" placeholder="Enter keyword..." data-error="Please Type some words..!">
							<div class="help-block with-errors"></div>
							<span class="classieraSearchLoader" style="display: none;">
							<img 
							style='display: none;' src="https://uklass.ng/carsales/wp-content/themes/classiera/images/loader.gif" alt="classiera loader"></span>
							
						</div>
					</div>					
					<!--Select Category-->
					<!--Locations-->
										<div class="form-group">
                       <div class="input-group inner-addon right-addon">
                            <div class="input-group-addon input-group-addon-width-sm"><i class="fa fa-map-marker"></i></div>
														<!--Locations dropdown-->	
									<select style="width: 240px;" id="classiera__loc" class="form-control form-control-sm sharp-edge select2-hidden-accessible" name="location" tabindex="-1" aria-hidden="true">
		<option value="all" selected="" disabled="">Select Location..</option>
		            
					@php 
					$tmpArr = [];
					foreach(\App\Car::all() as $c){
					    array_push($tmpArr, $c->location_state . ' State ' .$c->location_lga);
					}
					$tmpArr = array_unique($tmpArr);
					@endphp

					@foreach($tmpArr as $l)
					<option value="{{explode(' State ', $l)[1]}}">{{$l}}</option>
					@endforeach

					</select>
									<!--Locations dropdown-->
							                        </div>
                    </div>
										<!--Locations-->
					<!--PriceRange-->
										<div class="form-group clearfix">
						<div class="inner-addon right-addon">
							<i class="form-icon form-icon-size-small fa fa-sort"></i>
							<select class="form-control form-control-sm" data-placeholder="Select price range" name="price_range">
								<option value="-1" selected="" disabled="">select price range</option>
								<option value="0,20000000">0 - 20000000</option>
								<option value="20000000,40000000">20000001 - 40000000</option>
								<option value="40000000,60000000">40000001 - 60000000</option>
								<option value="60000000,80000000">60000001 - 80000000</option>
								<option value="80000000,100000000">80000001 - 100000000</option>
								<option value="100000000,120000000">100000001 - 120000000</option>
								<option value="120000000,140000000">120000001 - 140000000</option>
								<option value="140000000,160000000">140000001 - 160000000</option>
								<option value="160000000,180000000">160000001 - 180000000</option>
								<option value="180000000,200000000">180000001 - 200000000</option>
							</select>
						</div>
					</div>
										<!--PriceRange-->
					<div class="form-group">
						<button class="radius" style='background: black;' type="submit" name="search" value="Search">Search Now</button>
					</div>
				</form>
			</div><!--col-md-12-->
		</div><!--row-->
	</div><!--container-->
</section>
	
	
	
	
	
	
	
	
	
	
	
	
	
	<section class="search-section search-section-v5" style="background: #39444c;">
	<div class="container">
		<div class="row">
			<!--col-md-12-->
		</div><!--row-->
	</div><!--container-->
</section><!--search-section--><!-- page content -->
<section class="inner-page-content border-bottom top-pad-50">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-lg-9">
				<!--category description section-->
								<!--category description section-->
				<!--Google Section-->
								
								<!--Google Section-->
				<!-- advertisement -->
				<section class="classiera-advertisement advertisement-v1">
				
	
	<div class="tab-divs section-light-bg">
		<div class="view-head">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-6 col-xs-6">
						<div class="total-post">
							
						</div><!--total-post-->
					</div><!--col-lg-6-->
					<div class="col-lg-6 col-sm-6 col-xs-6">
						
					</div><!--col-lg-6-->
				</div><!--row-->
			</div><!--container-->
		</div><!--view-head-->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade active in" id="all">
				<div class="container">
                <div class="row">
                    

<iframe src="?request_slide=true" scrolling="no" style="width: 100%; height: 528px; border: 0px;"></iframe> 

<br/><br/>
<div class="border-section border details">
                        <h4 class="border-section-heading text-uppercase"><i class="fa fa-file-text-o"></i>Ad Description</h4>
                        <div class="post-details">
                            {{$car->description}}
                        </div>
</div><br/><br/>

<div class="border-section border details">
                        <h4 class="border-section-heading text-uppercase"><i class="fa fa-file-text-o"></i>Ad Details</h4>
                        <div class="post-details">
                            <ul class="list-unstyled clearfix">
								<li>
                                    <p>Ad ID: 
									<span class="pull-right flip">
										<i class="fa fa-hashtag IDIcon"></i>
										00{{$car->id}}									</span>
									</p>
                                </li><!--PostDate-->
                                <li>
                                    <p>Added: 
									<span class="pull-right flip">{{$car->created_at}}</span>
									</p>
                                </li><!--PostDate-->								
								<!--Price Section -->
								<li>
                                    <p>Distress Price: 
									<span class="pull-right flip">N{{$car->distress_price}}</p>
                                </li><!--Sale Price-->
																								<li>
                                    <p>Standard Price: 
									<span class="pull-right flip">										
									N{{$car->standard_price}}
									</span>
									</p>
                                </li><!--Regular Price-->
																<!--Price Section -->
																								<li>
                                    <p>Condition: 
									<span class="pull-right flip">{{$car->condition}}</span>
									</p>
                                </li><!--Condition-->
																								<li>
                                    <p>State: 
									<span class="pull-right flip">{{$car->location_state}}</span>
									</p>
                                </li><!--Location-->
																								<li>
                                    <p>LGA: 
									<span class="pull-right flip">{{$car->location_lga}}</span>
									</p>
                                </li><!--state-->
											
								<li>
                                    <p>Phone: 
									<span class="pull-right flip">
										<a href="tel:08038328183">08038328183</a>
									</span>
									</p>
                                </li><!--Phone-->
											
								<li>
                                    <p>Phone: 
									<span class="pull-right flip">
										<a href="tel:08084204010">08084204010</a>
									</span>
									</p>
                                </li><!--Phone-->
								
								<li>
                                    <p>Views: 
									<span class="pull-right flip">
										{{$car->views}}									</span>
									</p>
                                </li><!--Views-->
																			<li>
												<p>Vehicle Make: 
												<span class="pull-right flip">
													{{$car->brand}}												</span>
												</p>
											</li><!--test-->	
																							<li>
												<p>Model : 
												<span class="pull-right flip">
												{{$car->brand}}												</span>
												</p>
											</li><!--test-->	
																							<li>
												<p>Year: 
												<span class="pull-right flip">
												{{$car->year}}												</span>
												</p>
											</li><!--test-->	
																							<li>
												<p>Mileage: 
												<span class="pull-right flip">
												{{$car->mileage}}												</span>
												</p>
											</li><!--test-->	
											
											<!--<li>
												<p>Vehicle Color: 
												<span class="pull-right flip">
													Black												</span>
												</p>
											</li>--><!--test-->	
											
											<li>
												<p>Item Condition: 
												<span class="pull-right flip">
												{{$car->condition}}
												</span>
												</p>
											</li><!--dropdown-->
											                            </ul>
                        </div><!--post-details-->
                    </div>

					<br/><br/>
					<button onclick='bid();' class="btn btn-primary sharp btn-md btn-style-active">Buy Now</button>

                </div><!--row-->
				</div><!--container-->
			</div><!--tabpanel popular-->
		</div><!--tab-content-->
	</div><!--tab-divs-->
</section>				<!-- advertisement -->
			</div><!--col-md-8-->
			<div class="col-md-4 col-lg-3">
				<aside class="sidebar">
					<div class="row">
						<!--subcategory-->
												<!--subcategory-->
													<div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 179px;">
								
							</div>
												<div class="col-lg-12 col-md-12 col-sm-6 match-height" style="height: 179px;"><div class="widget-box"><div class="widget-title"><h4>Subscribe &amp; Follow</h4></div>		
		
		<div class="widget-content">
			<div class="social-network">
			  <a class="social-icon social-icon-sm footer-social" href="http://pinterest.com/pin/create/button/?url={{rawurlencode(url('/car/'.$car->id))}}&media={{$images[0]}}" target="_blank"><i class="fab fa-pinterest" aria-hidden="true"></i></a>
			  <a class="social-icon social-icon-sm footer-social" href="https://www.facebook.com/sharer/sharer.php?u={{rawurlencode(url('/car/'.$car->id))}}"><i class="fab fa-facebook" aria-hidden="true"></i></a>
			  <a class="social-icon social-icon-sm footer-social" href="http://twitter.com/share?text={{rawurlencode($car->title)}}&url={{rawurlencode(url('/car/'.$car->id))}}"><i class="fab fa-twitter" aria-hidden="true"></i></a>
			  <a class="social-icon social-icon-sm footer-social" href="https://www.linkedin.com/shareArticle?mini=true&url={{rawurlencode(url('/car/'.$car->id))}}"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
			  <a class="social-icon social-icon-sm footer-social" href="https://plus.google.com/share?url={{rawurlencode(url('/car/'.$car->id))}}"><i class="fab fa-google-plus" aria-hidden="true"></i></a>
			  <a class="social-icon social-icon-sm footer-social" href="whatsapp://send?text={{rawurlencode($car->title .' '. $car->description .' '. url('/car/'.$car->id))}}"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
			</div>
		</div>	
			
		</div></div>					</div><!--row-->
				</aside>
			</div><!--row-->
		</div><!--row-->
	</div><!--container-->
</section>	
<!-- page content -->
<footer class="section-pad section-bg-black section-bg-light-img">
	<!--container-->
</footer>
<section class="footer-bottom" style="margin-bottom: 68px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-sm-6">
				<p>All copyrights reserved © 2018 - Powered by <a href="/">Phakom.ng</a></p>
				13 Bode OLABODE street, Off Aboru Road, Iyana ipaja, Lagos state.
			</div><div class="col-lg-6 col-sm-6"><ul id="menu-footer-menu" class=""><a href="httpx://phakom.ng/carsales/category/cars/#"></a><li id="menu-item-371" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-371"><a title="Home" href="/">Home</a></li>



</ul>							</div>
		</div><!--row-->
	</div><!--container-->
</section>
	<!-- back to top -->
	<a href="#" id="back-to-top" title="Back to top" class="social-icon social-icon-md show"><i class="fa fa-angle-double-up removeMargin"></i></a><!--Start of Tawk.to Script (0.3.3)-->

	<style>
		.attachment:focus {
			outline: #1e8cbe solid;
		}
		.selected.attachment {
			outline: #1e8cbe solid;
		}
	</style>
	<!--[endif]---->
	<script type="text/html" id="tmpl-media-frame">
		<div class="media-frame-menu"></div>
		<div class="media-frame-title"></div>
		<div class="media-frame-router"></div>
		<div class="media-frame-content"></div>
		<div class="media-frame-toolbar"></div>
		<div class="media-frame-uploader"></div>
	</script>

	<script type="text/html" id="tmpl-media-modal">
		<div tabindex="0" class="media-modal wp-core-ui">
			<button type="button" class="media-modal-close"><span class="media-modal-icon"><span class="screen-reader-text">Close media panel</span></span></button>
			<div class="media-modal-content"></div>
		</div>
		<div class="media-modal-backdrop"></div>
	</script>

<script src="/theme-sublime/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/n-design_files/bootstrap.min.js"></script>
<script type="text/javascript" src="/n-design_files/bootstrap-dropdownhover.js"></script>
<script type="text/javascript" src="/n-design_files/validator.min.js"></script>
<script type="text/javascript" src="/n-design_files/owl.carousel.min.js"></script>
<script type="text/javascript" src="/n-design_files/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/n-design_files/infinitescroll.js"></script>
<script type="text/javascript" src="/n-design_files/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="/n-design_files/select2.min.js"></script>
<script type="text/javascript" src="/n-design_files/classiera.js"></script>
<script type="text/javascript" src="/n-design_files/jquery-ui.min.js"></script>
<script type="text/javascript" src="/n-design_files/classiera-map.js"></script>




<script src="/js/sweetalert.min.js"></script>
<form id='forms' style='display: none;' action='{{url("/bid/$car->id")}}' method='POST'>
     @csrf
     <input type='hidden' name='type' value='car'/>

	 @if($errors->has('bid_amount')) <small style='color: red;'>{{ $errors->first('bid_amount')}}</small> @endif
	 <input required='required' class="swal-content__input number" name='bid_amount' value='{{!empty($car->distress_price)?$car->distress_price:$car->standard_price}}'>
     <br/>

	 @if($errors->has('email')) <small style='color: red;'>{{ $errors->first('email')}}</small> @endif
	 <input required='required' class="swal-content__input" name='email' type='email' placeholder='email' value='{{old("email")}}'>
	 <br/>

	 @if($errors->has('phone')) <small style='color: red;'>{{ $errors->first('phone')}}</small> @endif
	 <input required='required' class="swal-content__input" name='phone' type='phone' placeholder='phone' value='{{old("phone")}}'>

	 <script src='{{ url("/js/number-formatter.js") }}'></script>
  </form>
<script>
	  function bid() {
		  var forms = document.getElementById('forms');
		  forms.style.display = 'block';
		  swal({
			  text: "Your purchase/bid amount (₦):",
			  content: forms,
			  buttons: {
				  confirm: {
					text: 'Submit'
				  },
				},
			})
			.then((value) => {
				if(value){
					document.getElementById('forms').submit();
				}
			});
		}
		
		@if(session('status'))
		   swal("Bid Placed", "Thanks. You will be contacted shortly.");
		@endif
  </script>

    @if (!$errors->isEmpty())
        <script>
        (function() {
            bid();
        })();
        </script>
    @endif




  
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bc4a06661d0b770925148ce/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


</body></html>
@endif