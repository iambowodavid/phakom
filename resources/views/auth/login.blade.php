@extends('layouts.app')

@section('content')


<div class="container-fluid ">
        <div class ="row form-header">
    
    
	
        </div>
		
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card form-card">
                   
                    <p class ="form-heading">Login</p>
                   
	
				
				
				@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
				
				
				
				
                    <div class="card-body">
                            <form method="POST" action="{{ route('login') }}" id ="login-form">
                            @csrf
    
                            <div class ="row">	
                                    <div class="form-group col-md-12">
                                            <label for="email" class="col-sm-12 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
                
                                            <div class="col-md-12">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                    </div>
                        </div>
    
    
                        <div class ="row">
                                <div class="form-group col-md-12">
                                        <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Password') }}</label>
            
                                        <div class="col-md-12">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
    
                           
                        </div>

                        <div class ="row">


                                <div class="form-group col-md-6" style="display: none">
                                        <div class="col-md-12">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                                </label>
                                            </div>
                                        </div>
                                </div>

                                <div class="form-group col-md-12">
                                        
                                </div>


                        </div>
    
                            <div class="form-group row mb-0">
                                <div class="form-submit">
                                        <button type="submit" class="btn btn-primary">
                                                {{ __('Login') }}
                                            </button>
                                    
                                </div>
                                
                            </div>
    
                            <p class ="form-meta">Don't have an account ? <a class="" href="{{ url('/register') }}">{{ __('Register') }}</a></p>
							</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('footer')
@endsection
