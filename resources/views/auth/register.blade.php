@extends('layouts.app')

@section('content')

<div class="container-fluid" id ="register-js">
    <div class ="row form-header">
	
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card form-card">
               
                <p class ="form-heading">Create your account with {{ config("app.name") }}</p>
               
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" id ="register-form">
                        @csrf

                    <div class ="row">
                          
						  <div class="form-group col-md-6">
                             <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Username') }}</label>

                              <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                             </div>
                          </div>

						  <div class="form-group col-md-6">
                             <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Phone Number') }}</label>

                              <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" required autofocus>
								
                                @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                             </div>
                          </div>
						  
                        <div class="form-group col-md-6">
                            <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
						
						
		
						
						
						
					<div class ="row">	
						<div class="form-group col-md-6">
                            <label for="first_name" class="col-md-12 col-form-label text-md-left">{{ __('First Name') }}</label>

                            <div class="col-md-12">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
							
						
						<div class="form-group col-md-6">
                            <label for="last_name" class="col-md-12 col-form-label text-md-left">{{ __('Last Name') }}</label>

                            <div class="col-md-12">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
					</div>
						
						
					
                    <div class ="row">
                        <div class="form-group col-md-6">
                            <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password-confirm" class="col-md-12 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>

                        <div class="form-group row mb-0">
                            <div class="form-submit">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                
                            </div>
                           
                        </div>

                        <p class ="form-meta">Already have an account ? <a class="" href="{{ url('/login') }}">{{ __('Login') }}</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
@endsection
