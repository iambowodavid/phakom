
@php $images = App\Http\Controllers\Admin\ManageUploads::getFilesURLs($car->photos_id); @endphp

@if(isset($_GET['request_slide']))
<!DOCTYPE html>
<html lang="en">

<head>
    <script>
        var SITE_URL = 'http://wowslider.com/';
    </script>
    <script type="text/javascript" src="http://wowslider.com/images/demo/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="http://wowslider.com/sliders/demo-88/engine1/style.css" />
</head>

<body>

    <!-- Start WOWSlider.com BODY section -->
    <div id="wowslider-container1">
        <div class="ws_images">
            <ul>
				@foreach($images as $source)
                <li><img src="{{$source}}" alt="slideshow" title="{{$car->title}}" id="wows1_0" />{{$car->title}}</li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="ws_shadow"></div>

    <script type="text/javascript" src="http://wowslider.com/images/demo/wowslider.js"></script>
    <script>
        jQuery('#wowslider-container1').wowSlider({
            effect: "cube_over",
            prev: "",
            next: "",
            duration: 2 * 1000,
            delay: 5 * 1000,
            width: 960,
            height: 500,
            autoPlay: true,
            autoPlayVideo: false,
            playPause: false,
            stopOnHover: false,
            loop: false,
            bullets: 1,
            caption: true,
            captionEffect: "parallax",
            controls: true,
            responsive: 1,
            fullScreen: false,
            gestures: 2,
            onBeforeStep: 0,
            images: 0,
            staticColor: true
        });
    </script>
    <!-- End WOWSlider.com BODY section -->

</body>

</html>

@else
<!DOCTYPE html>
<html lang="en">
<head>
<title>Phakom.ng | {{$car->title}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/theme-sublime/styles/bootstrap4/bootstrap.min.css">
<link href="/theme-sublime/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/theme-sublime/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/theme-sublime/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="/theme-sublime/plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="/theme-sublime/styles/product.css">
<link rel="stylesheet" type="text/css" href="/theme-sublime/styles/product_responsive.css">
</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo"><a href="#">PHAKOM.ng</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	

	
	
	<!-- Home -->

	<div class="home">
		<div class="home_container">
            <iframe src="?request_slide=true" scrolling="no" style="width: 100%; height: 528px; border: 0px;"></iframe> 
		</div>
    </div>
    
    <p style='padding-top:50px;'></p>

	<!-- Product Details -->

	<div class="product_details">
		<div class="container">
			<div class="row details_row">

				<!-- Product Image -->
				<!--<div class="col-lg-6">
					<div class="details_image">
						<div class="details_image_large"><img style='min-width:300px;' src="{{$images[0]}}" alt=""><div class="product_extra product_new"><a href='#'>{{$car->condition}}</a></div></div>
					</div>
				</div>-->

				<!-- Product Content -->
				<div class="col-lg-6">
					<div class="details_content">
						<div class="details_name">{{$car->title}}</div>
						<div class="details_discount">&#8358;{{$car->standard_price}}</div>
						<div class="details_price">&#8358;{{$car->distress_price}}</div>

						<!-- In Stock -->
						<div class="in_stock_container">

							<div class="availability">Brand:</div>
							<span>{{$car->brand}}</span>

							<br/>
							<div class="availability">Model:</div>
							<span>{{$car->model}}</span>

							<br/>
							<div class="availability">Year:</div>
							<span>{{$car->year}}</span>

						</div>
						<div class="details_text">
							<p>{{$car->description}}</p>
						</div>

						<!-- Product Quantity -->
						<div class="product_quantity_container">
							
							<div class="button cart_button"><a href="#" onclick='bid(); return false;'>Buy Now</a></div>
						</div>

						<!-- Share -->
						<div class="details_share">
							<span>Share:</span>
							<ul>
								<li><a href="http://pinterest.com/pin/create/button/?url={{rawurlencode(url('/car/'.$car->id))}}&media={{$images[0]}}"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								<!--<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
								<li><a href="https://www.facebook.com/sharer/sharer.php?u={{rawurlencode(url('/car/'.$car->id))}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="http://twitter.com/share?text={{rawurlencode($car->title)}}&url={{rawurlencode(url('/car/'.$car->id))}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{rawurlencode(url('/car/'.$car->id))}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								<li><a href="https://plus.google.com/share?url={{rawurlencode(url('/car/'.$car->id))}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<li><a href="whatsapp://send?text={{rawurlencode(url('/car/'.$car->id))}}"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="row description_row">
				<div class="col">
					
					
				</div>
			</div>
		</div>
	</div>
	
</div>

<script src="/theme-sublime/js/jquery-3.2.1.min.js"></script>
<script src="/theme-sublime/styles/bootstrap4/popper.js"></script>
<script src="/theme-sublime/styles/bootstrap4/bootstrap.min.js"></script>
<script src="/theme-sublime/plugins/greensock/TweenMax.min.js"></script>
<script src="/theme-sublime/plugins/greensock/TimelineMax.min.js"></script>
<script src="/theme-sublime/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="/theme-sublime/plugins/greensock/animation.gsap.min.js"></script>
<script src="/theme-sublime/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="/theme-sublime/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="/theme-sublime/plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="/theme-sublime/plugins/easing/easing.js"></script>
<script src="/theme-sublime/plugins/parallax-js-master/parallax.min.js"></script>
<script src="/theme-sublime/js/product.js"></script>

  <script src="/js/sweetalert.min.js"></script>
  <form id='forms' style='display: none;' action='{{url("/bid/$car->id")}}' method='POST'>
     @csrf
     <input type='hidden' name='type' value='car'/>

	 @if($errors->has('bid_amount')) <small style='color: red;'>{{ $errors->first('bid_amount')}}</small> @endif
	 <input required='required' class="swal-content__input number" name='bid_amount' value='{{!empty($car->distress_price)?$car->distress_price:$car->standard_price}}'>
     <br/>

	 @if($errors->has('email')) <small style='color: red;'>{{ $errors->first('email')}}</small> @endif
	 <input required='required' class="swal-content__input" name='email' type='email' placeholder='email' value='{{old("email")}}'>
	 <br/>

	 @if($errors->has('phone')) <small style='color: red;'>{{ $errors->first('phone')}}</small> @endif
	 <input required='required' class="swal-content__input" name='phone' type='phone' placeholder='phone' value='{{old("phone")}}'>

	 <script src='{{ url("/js/number-formatter.js") }}'></script>
  </form>
  <script>
	  function bid() {
		  var forms = document.getElementById('forms');
		  forms.style.display = 'block';
		  swal({
			  text: "Your purchase/bid amount (₦):",
			  content: forms,
			  buttons: {
				  confirm: {
					text: 'Submit'
				  },
				},
			})
			.then((value) => {
				if(value){
					$('#forms').submit();
				}
			});
		}
		
		@if(session('status'))
		   swal("Bid Placed", "Thanks. You will be contacted shortly.");
		@endif
  </script>

    @if (!$errors->isEmpty())
        <script>
        (function() {
            bid();
        })();
        </script>
    @endif

</body>

</html>
@endif