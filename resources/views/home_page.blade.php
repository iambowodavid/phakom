<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Phakom.ng</title>
    	<meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 
Power Template 
http://www.templatemo.com/tm-508-power
-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        
        <link rel="stylesheet" href="/home-page/css/bootstrap.min.css">
        <link rel="stylesheet" href="/home-page/css/font-awesome.css">
        <link rel="stylesheet" href="/home-page/css/animate.css">
        <link rel="stylesheet" href="/home-page/css/templatemo_misc.css">
        <link rel="stylesheet" href="/home-page/css/templatemo_style.css">
        <link rel="stylesheet" href="/home-page/css/owl-carousel.css">

        <script src="/home-page/js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a onclick='return false;' href="http://browsehappy.com/">Upgrade your browser today</a> or <a onclick='return false;' href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->


        <div class="site-main" id="sTop">
            <div class="site-header">
                <div class="main-header">
                    <div class="container">
                        <div id="menu-wrapper">
                            <div class="row">
                                <nav class="navbar navbar-inverse" role="navigation">
                                    <div class="navbar-header">
                                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a href="#top" class="navbar-brand">Phakom.ng</a>
                                        <div id="main-nav" class="collapse navbar-collapse">
                                            <ul class="menu-first nav navbar-nav" style="margin-right: 20px;">
                                                <!--<li class="active"><a href="/cars">Cars</a></li> 
                                                <li><a href="/lands">Lands</a></li>                 
                                                <li><a href="/housings">Housing</a></li>-->        
                                                <li class="active"><a href="{{url('/contact-us/')}}">Contact Us</a></li>          
                                                <li><a href="{{url('/aboutus/')}}">About Us</a></li>               
                                                <li><a href="{{url('/admin/manage/car/-1')}}">Sell A Car</a></li>                    
                                                <li><a href="{{url('/admin/manage/housing/-1')}}">Sell/Rent A House</a></li>                      
                                                <li><a href="{{url('/admin/manage/land/-1')}}">Sell Land</a></li>                      
                                                @if(!\Auth::check())<li><a href="{{url('/login')}}">Login/Register</a></li>@endif
                                                <!--
                                                    <li><a href="{{url('/sell/car/-1')}}">Sell A Car</a></li>
                                                    <li><a href="{{url('/sell-rent/house/-1')}}">Sell/Rent A House</a></li>
                                                    <li><a href="{{url('/sell/land/-1')}}">Sell Land</a></li> 
                                                -->                                  
                                            </ul>                                    
                                        </div> <!-- /.main-menu -->
                                    </div>
                                </nav>
                            </div> <!-- /.row -->
                        </div> <!-- /#menu-wrapper -->                        
                    </div> <!-- /.container -->
                </div> <!-- /.main-header -->
            </div> <!-- /.site-header -->
        </div> <!-- /.site-main -->


        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="banner-content">
                            <h6>Welcome To</h6>
                            <h2 style='font-size: 50px;'>Phakom.ng</h2>
                            <ul class="buttons">
                                <li>
                                    <div class="primary-button">
                                        <a href="/cars">Buy Cars</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="secondary-button">
                                        <a href="/lands">Lands</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="primary-button">
                                        <a href="/housings">Housings</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="intro">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Phakom.ng</h4>
                        <h6>Phakom Global Investments LTD.</h6>
                    </div>
                </div>
            </div>
        </div>


        <section id="about" class="page-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="service-item first-service">
                            <a href='/cars'>
							
							  <i class='fa fa-car' style='font-size:60px; color: white;'></i>
                              <h4>Buy Cars</h4>
                              <p>Click here to buy Cars easily.</p>
							
							</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="service-item second-service">
                            <a href='/lands'>
							
							  <i class='fa  fa-map-marker' style='font-size:60px; color: white;'></i>
                              <h4>Lands</h4>
                              <p>Click here to Buy lands easily.</p>
							
							</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="service-item third-service">
                            <a href='/housings'>
							
							  <i class='fa fa-home' style='font-size:60px; color: white;'></i>
                               <h4>Housing</h4>
                               <p>Click here buy/rent houses easily.</p>
							   
							</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<iframe src="testimonials.html" id="preview" class="preview" scrolling="no" style="pading-top: 50px; width:100%; height: 528px;"></iframe>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                            <p>Copyright &copy; @php echo date("Y"); @endphp Phakom Global Investments LTD. <br>
                            13 Bode OLABODE street, Off Aboru Road, Iyana ipaja, Lagos state.
					</div>
                    <div class="col-md-4 col-sm-12">
                        <ul class="social-icons">
                            <li><a href="http://facebook.com/Phakom/">Facebook</a></li>
                            <li><a href="http://twitter.com/Phakomng">Twitter</a></li>
                            <li><a href="http://linkedin.com/in/phakom">Linkedin</a></li>
                            <li><a href="http://instagram.com/Phakom.ng/">Instagram</a></li>
                            <li><a href="http://plus.google.com/101954945785618918186">Google+</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 col-md-offset-2 col-sm-12">
                        <div class="back-to-top">
                            <a href="#top">
                                <i class="fa fa-angle-up"></i>
                                back to top
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
        <script src="/home-page/js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="/home-page/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="/home-page/js/bootstrap.js"></script>
        <script src="/home-page/js/plugins.js"></script>
        <script src="/home-page/js/main.js"></script>

        <!-- Google Map -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="/home-page/js/vendor/jquery.gmap3.min.js"></script>

        <script type="text/javascript">
        $(document).ready(function() {
            
            // mobile nav toggle
            $('#nav-toggle').on('click', function (event) {
                event.preventDefault();
                $('#main-nav').toggleClass("open");
            });
        });
        </script>
        
        <!-- templatemo 406 flex -->

        <script>
        
        </script>
        
        <script src="/js/sweetalert.min.js"></script>
    <form id='forms' style='display: none;' action='{{url("/subscribe-to-newsletter")}}' method='POST'>
    <img src='/subscribe-popup/images/responsive.png'>
    <h2>Subscribe to the Phakom Newsletter Now!</h2>
     @csrf
	 <input required='required' class="swal-content__input" name='email' id='email' type='email' placeholder='email'>
	 <br/>

	 <input required='required' class="swal-content__input" name='name' id='name' type='text' placeholder='name'>
  </form>
<script>
	  function subscribe() {
		  var forms = document.getElementById('forms');
		  forms.style.display = 'block';
		  swal({
			  //text: ":",
			  content: forms,
			  buttons: {
				  confirm: {
					text: 'Submit'
				  },
				},
			})
			.then((value) => {
				if(value && document.getElementById('name').value !== '' && document.getElementById('email').value !== ''){
					document.getElementById('forms').submit();
				}
			});
		}
		@if(session('status'))
		   swal("Subscribed!", "Thanks for subscribing.");
        @elseif($request->cookie('subscribed-to-newsletter') === NULL)
           setTimeout(function(){subscribe();}, 5000);
		@endif
  </script>


        </body>
</html>