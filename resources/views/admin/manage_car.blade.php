@include('admin.head_scripts')

<body class="nav-md">

    <div class="container body" style='background-color: white;'>

        <div class="main_container">

    @if (session('status'))
    <div class="alert alert-success" align="center">
        {{ session('status') }}
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>swal("Success.", "{{ session('status') }}", "success");</script>
    </div>
    @endif
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3 style='color: #cc0000;'>Phakom</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Cars</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content" id="main-form-container">
                                    <br />

									@include('admin.multi_upload')

                                    @php if(!empty($car->photos_id) && is_dir(public_path("/photos/$car->photos_id"))){@endphp
                                    <div class="image_preview" style='border: 1px solid black; padding: 10px;'>
                                      @foreach(scandir(public_path("/photos/$car->photos_id")) as $filePath)
                                        @php if($filePath === '.' || $filePath === '..'){continue;} @endphp
                                        <div style='min-width: 100px; overflow: hidden; display: inline-block;'>
                                            <img src='{{url("/photos/$car->photos_id/$filePath")}}' style='width: 100px;height: 100px;padding: 5px;'/>
                                        </div>;
                                      @endforeach
                                    </div>
                                    @php }@endphp

                                    <form id='main-form' enctype="multipart/form-data" method='POST' data-parsley-validate class="form-horizontal form-label-left" onsubmit='return checkUpload()'>
                                        @csrf
                                        
                                        <input type='hidden' name='proposed-id' value='{{ $car->id }}'/>
                                        <input type='hidden' name='photos_id' id='photos_id' value='{{ \Carbon\Carbon::now()->timestamp }}'/>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post-title">Post Title <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name='title' type="text" id="title" required="required" class="form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('title')) ? old('title') : $car->title }}">
                                                    
                                                    @if ($errors->has("title"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("title") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="brand">Category <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id='category' name='category' class="form-control x">
                                                    <option value="" selected="selected">- Select -</option>
                                                    <option value="cars">Cars</option>
                                                    <option value="motorcycles">Motorcycles</option>
                                                    <option value="trucks">Trucks</option>
                                                    <option value="buses">Buses</option>
                                                    <option value="agricultural_vehicles">Agricultural Vehicles</option>
                                                    <option value="">Uncategorized</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="textarea" required="required" name="description" class="form-control col-md-7 col-xs-12">{{ !empty(old('description')) ? old('description') : $car->description }}</textarea>
                                                    
                                                    @if ($errors->has("description"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("description") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>
										
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="brand">Brand <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id='brand' name='brand' class="select2_single form-control x">
                                                    <option value="" selected="selected">- Select -</option>
                                                    <option value="Acura">Acura</option>
                                                    <option value="Alfa Romeo">Alfa Romeo</option>
                                                    <option value="AMC">AMC</option>
                                                    <option value="Aston Martin">Aston Martin</option>
                                                    <option value="Audi">Audi</option>
                                                    <option value="Austin">Austin</option>
                                                    <option value="Austin Healey">Austin Healey</option>
                                                    <option value="Bentley">Bentley</option>
                                                    <option value="BMW">BMW</option>
                                                    <option value="Buick">Buick</option>
                                                    <option value="Cadillac">Cadillac</option>
                                                    <option value="Chevrolet">Chevrolet</option>
                                                    <option value="Chrysler">Chrysler</option>
                                                    <option value="Citroen">Citroen</option>
                                                    <option value="Cord">Cord</option>
                                                    <option value="Daewoo">Daewoo</option>
                                                    <option value="Datsun">Datsun</option>
                                                    <option value="DeLorean">DeLorean</option>
                                                    <option value="DeSoto">DeSoto</option>
                                                    <option value="Dodge">Dodge</option>
                                                    <option value="Eagle">Eagle</option>
                                                    <option value="Edsel">Edsel</option>
                                                    <option value="Ferrari">Ferrari</option>
                                                    <option value="Fiat">Fiat</option>
                                                    <option value="Ford">Ford</option>
                                                    <option value="Geo">Geo</option>
                                                    <option value="GMC">GMC</option>
                                                    <option value="Honda">Honda</option>
                                                    <option value="Hummer">Hummer</option>
                                                    <option value="Hyundai">Hyundai</option>
                                                    <option value="Infiniti">Infiniti</option>
                                                    <option value="International Harvester">International Harvester</option>
                                                    <option value="Jaguar">Jaguar</option>
                                                    <option value="Jeep">Jeep</option>
                                                    <option value="Kia">Kia</option>
                                                    <option value="Lamborghini">Lamborghini</option>
                                                    <option value="Lancia">Lancia</option>
                                                    <option value="Land Rover">Land Rover</option>
                                                    <option value="Lexus">Lexus</option>
                                                    <option value="Lincoln">Lincoln</option>
                                                    <option value="Lotus">Lotus</option>
                                                    <option value="Maserati">Maserati</option>
                                                    <option value="Mazda">Mazda</option>
                                                    <option value="Mercedes-Benz">Mercedes-Benz</option>
                                                    <option value="Mercury">Mercury</option>
                                                    <option value="MG">MG</option>
                                                    <option value="Mini">Mini</option>
                                                    <option value="Mitsubishi">Mitsubishi</option>
                                                    <option value="Nash">Nash</option>
                                                    <option value="Nissan">Nissan</option>
                                                    <option value="Oldsmobile">Oldsmobile</option>
                                                    <option value="Opel">Opel</option>
                                                    <option value="Packard">Packard</option>
                                                    <option value="Peugeot">Peugeot</option>
                                                    <option value="Plymouth">Plymouth</option>
                                                    <option value="Pontiac">Pontiac</option>
                                                    <option value="Porsche">Porsche</option>
                                                    <option value="Renault">Renault</option>
                                                    <option value="Rolls-Royce">Rolls-Royce</option>
                                                    <option value="Saab">Saab</option>
                                                    <option value="Saturn">Saturn</option>
                                                    <option value="Scion">Scion</option>
                                                    <option value="Shelby">Shelby</option>
                                                    <option value="Studebaker">Studebaker</option>
                                                    <option value="Subaru">Subaru</option>
                                                    <option value="Suzuki">Suzuki</option>
                                                    <option value="Toyota">Toyota</option>
                                                    <option value="Triumph">Triumph</option>
                                                    <option value="Volkswagen">Volkswagen</option>
                                                    <option value="Volvo">Volvo</option>
                                                    <option value="Willys">Willys</option>
                                                    <option value="Alpina">Alpina</option>
                                                    <option value="Asia Motors">Asia Motors</option>
                                                    <option value="Bugatti">Bugatti</option>
                                                    <option value="Corvette">Corvette</option>
                                                    <option value="Dacia">Dacia</option>
                                                    <option value="Dacia">Daihatsu</option>
                                                    <option value="DeTomaso">DeTomaso</option>
                                                    <option value="Isuzu">Isuzu</option>
                                                    <option value="Lada">Lada</option>
                                                    <option value="Ligier">Ligier</option>
                                                    <option value="Maybach">Maybach</option>
                                                    <option value="Morgan">Morgan</option>
                                                    <option value="Morris">Morris</option>
                                                    <option value="Rover">Rover</option>
                                                    <option value="Seat">Seat</option>
                                                    <option value="Skoda">Skoda</option>
                                                    <option value="Smart">Smart</option>
                                                    <option value="Ssangyong">Ssangyong</option>
                                                    <option value="Talbot">Talbot</option>
                                                    <option value="TVR">TVR</option>
                                                    <option value="Vauxhall">Vauxhall</option>
                                                    <option value="AIXAM">AIXAM</option>
                                                    <option value="Alpine">Alpine</option>
                                                    <option value="Autobianchi">Autobianchi</option>
                                                    <option value="Catherham">Catherham</option>
                                                    <option value="Ford (Europe)">Ford (Europe)</option>
                                                    <option value="GAZ">GAZ</option>
                                                    <option value="Great Wall">Great Wall</option>
                                                    <option value="Holden">Holden</option>
                                                    <option value="Jensen">Jensen</option>
                                                    <option value="Marcos">Marcos</option>
                                                    <option value="Mclaren">Mclaren</option>
                                                    <option value="Proton">Proton</option>
                                                    <option value="Tata">Tata</option>
                                                    <option value="Trabant">Trabant</option>
                                                    <option value="Venturi">Venturi</option>
                                                    <option value="ZIL">ZIL</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model">Model <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="model" name="model" required="required" class="form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('model')) ? old('model') : $car->model }}">

                                                    @if ($errors->has("model"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("model") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mileage">Year <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" id="year" name="year" required="required" class="form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('year')) ? old('year') : $car->year }}">

                                                    @if ($errors->has("year"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("year") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="brand">Type of car <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id='car_type' name='car_type' class="form-control x">
                                                    <option value="" selected="selected">- Select -</option>
                                                    <option value="2 door">2 door</option>
                                                    <option value="4 door">4 door</option>
                                                    <option value="SUV">Sport Utility Vehicle (SUV)</option>
                                                    <option value="Van/Minivan">Van / Minivan</option>
                                                    <option value="Pickup truck">Pickup truck</option>
                                                    <option value="Convertible">Convertible</option>
                                                </select>
                                            </div>
                                        </div>

                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Condition *:</label>
                                        <p>
                                            New:
                                            <input type="radio" class="flat" name="condition" id="new" value="new" x />
                                            Used:
                                            <input type="radio" class="flat" name="condition" id="used" value="used" />
                                            Tokunbo:
                                            <input type="radio" class="flat" name="condition" id="tokunbo" value="tokunbo" />
                                        </p>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mileage">Mileage (km) <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="mileage" name="mileage" required="required" class="number form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('mileage')) ? old('mileage') : $car->mileage }}">

                                                    @if ($errors->has("mileage"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("mileage") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Transmission *:</label>
                                        <p>
                                            Manual:
                                            <input type="radio" class="flat" name="transmission" id="manual" value="manual" x /> Automatic:
                                            <input type="radio" class="flat" name="transmission" id="automatic" value="automatic" />
                                        </p>

                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Car registered *:</label>
                                        <p>
                                            Yes:
                                            <input type="radio" class="flat" name="car_registered" id="yes" value="yes" x /> No:
                                            <input type="radio" class="flat" name="car_registered" id="no" value="no" />
                                        </p>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="standard_price">Standard price &#8358;<span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="standard_price" name="standard_price" required="required" class="number form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('standard_price')) ? old('standard_price') : $car->standard_price }}">

                                                    @if ($errors->has("standard_price"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("standard_price") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="distress_price">Distress price &#8358;<span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="distress_price" name="distress_price" class="number form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('distress_price')) ? old('distress_price') : $car->distress_price }}">

                                                    @if ($errors->has("distress_price"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("distress_price") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Distress Ending Date <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input autocomplete="off" name="distress_ending_date" id="distress_ending_date" class="date-picker form-control col-md-7 col-xs-12" type="text"
                                                    value="{{ !empty(old('distress_ending_date')) ? old('distress_ending_date') : $car->distress_ending_date }}">

                                                    @if ($errors->has("distress_ending_date"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("distress_ending_date") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <h2 align='center'>Location:</h2>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">State <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="location_state" id="state" class="form-control">
                                                    <option value="" selected="selected">- Select -</option>
                                                    <option value='Abia'>Abia</option>
                                                    <option value='Adamawa'>Adamawa</option>
                                                    <option value='AkwaIbom'>AkwaIbom</option>
                                                    <option value='Anambra'>Anambra</option>
                                                    <option value='Bauchi'>Bauchi</option>
                                                    <option value='Bayelsa'>Bayelsa</option>
                                                    <option value='Benue'>Benue</option>
                                                    <option value='Borno'>Borno</option>
                                                    <option value='Cross River'>Cross River</option>
                                                    <option value='Delta'>Delta</option>
                                                    <option value='Ebonyi'>Ebonyi</option>
                                                    <option value='Edo'>Edo</option>
                                                    <option value='Ekiti'>Ekiti</option>
                                                    <option value='Enugu'>Enugu</option>
                                                    <option value='FCT'>FCT</option>
                                                    <option value='Gombe'>Gombe</option>
                                                    <option value='Imo'>Imo</option>
                                                    <option value='Jigawa'>Jigawa</option>
                                                    <option value='Kaduna'>Kaduna</option>
                                                    <option value='Kano'>Kano</option>
                                                    <option value='Katsina'>Katsina</option>
                                                    <option value='Kebbi'>Kebbi</option>
                                                    <option value='Kogi'>Kogi</option>
                                                    <option value='Kwara'>Kwara</option>
                                                    <option value='Lagos'>Lagos</option>
                                                    <option value='Nasarawa'>Nasarawa</option>
                                                    <option value='Niger'>Niger</option>
                                                    <option value='Ogun'>Ogun</option>
                                                    <option value='Ondo'>Ondo</option>
                                                    <option value='Osun'>Osun</option>
                                                    <option value='Oyo'>Oyo</option>
                                                    <option value='Plateau'>Plateau</option>
                                                    <option value='Rivers'>Rivers</option>
                                                    <option value='Sokoto'>Sokoto</option>
                                                    <option value='Taraba'>Taraba</option>
                                                    <option value='Yobe'>Yobe</option>
                                                    <option value='Zamfara'>Zamafara</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">LGA <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select style='padding-top: 10px;' name="location_lga" id="lga" class="form-control" x></select>
                                            </div>
                                        </div>
										
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <a href='{{ url("admin") }}' class="btn btn-warning">Go Back</a>
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                <button type="submit" class="btn btn-success">Save Ad</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#distress_ending_date').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4",
								locale: {
									format: 'DD-MM-YYYY'
								}
                            }, function(start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>

                </div>

            </div>

        </div>
        <!-- /page content -->
    </div>

    </div>
    </div>

    <script>
       @php $location_state = $car->location_state; @endphp
       @php $location_lga = $car->location_lga; @endphp
       @include('admin.lga_js')
       
       var category = '{{ $car->category }}';
       var brand = '{{ $car->brand }}';
       var car_type = '{{ $car->car_type }}';
       var condition = '{{ $car->condition }}';
       var transmission = '{{ $car->transmission }}';
       var registered = '{{ $car->car_registered }}';
       
       $('#brand').val(brand);
       $('#car_type').val(car_type);
       $('#category').val(category);

       $('#'+condition).attr('checked', '');
       $('#'+transmission).attr('checked', '');
       $('#'+registered).attr('checked', '');
    </script>

    <script src='{{ url("/js/number-formatter.js") }}'></script>

	@include('admin.footer_scripts')
	
</body>

</html>