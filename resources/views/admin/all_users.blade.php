@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('admin.sidebar')

         <div class ="col-md-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 15px; margin: 0px 0px 30px 0px; display: flex; justify-content: space-between;">

                                    <div class ="heading" style="padding-top: 6px">All Users</div>
                                    


                                  </div>

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
                                   <div class ="table-responsive">
                                    <table class="table" id ="users-table">
                                   
                                     <thead>
                                       <tr>
                                          <th>User name</th>
                                          <th>Email Address</th>
                                          <!--<th>Phone Number</th>
                                          <th>Total Investment</th>-->
                                          
                                       </tr>
                                     </thead>
                                     <tbody>
                                        @foreach ($users as $user)
                                         <tr>
                                           <td>{{ $user->name }}</td>
                                           <td>{{ $user->email }}</td>
                                           <td>{{ $user->phone_number }}</td>
                                           <td><a href = 'users/{{ $user->id }}' ><i class="fa fa-user text-info"></i></a></td>
                                           
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    
</div>
@endsection
