@include('admin.head_scripts')

<body class="nav-md">

    <div class="container body" style='background-color: white;'>

        <div class="main_container">

    @if (session('status'))
    <div class="alert alert-success" align="center">
        {{ session('status') }}
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>swal("Success.", "{{ session('status') }}", "success");</script>
    </div>
    @endif
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3 style='color: #cc0000;'>Phakom Dashboard</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Cars</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content" id="main-form-container">
                                    <h1 align='center'>{{\App\NewsletterSubscriber::all()->count()}} Subscribers</h1><br/>
                                    <form id='main-form' enctype="multipart/form-data" method='POST' data-parsley-validate class="form-horizontal form-label-left" onsubmit='return checkUpload()'>
                                        @csrf
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post-title">Email Title <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name='title' type="text" id="title" required="required" class="form-control col-md-7 col-xs-12"
                                                    value="{{ old('title') }}">
                                                    
                                                    @if ($errors->has("title"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("title") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Message <span class="x">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea rows="10" id="textarea" required="required" name="message" class="form-control col-md-7 col-xs-12">{{ old('description') }}</textarea>
                                                    
                                                    @if ($errors->has("description"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("description") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>
										
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <a href='{{ url("admin") }}' class="btn btn-warning">Go Back</a>
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                <button type="submit" class="btn btn-success">Send</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <!-- /page content -->
    </div>

    </div>
    </div>

    
	@include('admin.footer_scripts')
	
</body>

</html>