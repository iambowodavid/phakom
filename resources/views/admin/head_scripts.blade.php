<!DOCTYPE html>
<html lang="en" style='background-color: white;'>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Phakom | Dashboard</title>

    <!-- Bootstrap core CSS -->

    <link href="/panel/css/bootstrap.min.css" rel="stylesheet">

    <link href="/panel/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="/panel/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="/panel/css/custom.css" rel="stylesheet">
    <link href="/panel/css/icheck/flat/green.css" rel="stylesheet">
	
	<!-- SCRIPTS -->
    <script src="/panel/js/jquery.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
</head>