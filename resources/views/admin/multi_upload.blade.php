
            <style type="text/css">
                input[type=file] {
                    display: inline;
                }
                
                .image_preview {
                    border: 1px solid black;
                    padding: 10px;
                }
                
                .image_preview img {
                    width: 100px;
					height: 100px;
                    padding: 5px;
                }
            </style>


<div class="form-group" style='display: ;'>

<img src='' id='preview'><img src='' id='original'>
    <div class="x_panel">
        <div class="x_title">
            <h2>Unlimited photos uploader</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_contentx">

			<div style='min-width: 100px; overflow: hidden; display: inline-block;'>
				
				<b>Click to upload.</b><br/>
				
				<i class='fa fa-camera' style="font-size: 100px; color: #26B99A; height: 100px; width: 100px; position: absolute;"></i>
				
				<input data-v-4de3fe2f="" name="uploadFile[]" id="uploadFile" accept="image/*" multiple="multiple" class="btn" style="opacity: 0; height: 100px; width: 100px;" type="file">
				
				
			</div>

            <br/>

            <div id="image_preview" class="image_preview"></div>
        </div>
    </div>
</div>

<script type = "text/javascript">

var previewHolder = $('#image_preview');

var uploadFile = $("#uploadFile");

var uploadFileNative = document.getElementById("uploadFile");

function clearImg(imgIndex) {
    $("#img-" + imgIndex).remove();
}

uploadFile.change(function() {
    previewHolder.html("");

    var all_files = uploadFileNative.files;

    var total_files = all_files.length;

    if(total_files > 0)
    {
        loadImageFile(total_files, 0);
    }
});

var loadImageFile = function(total_files, index) {
    if(total_files == index){return}

    var fileReader = new FileReader();
    var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
    
    
    
    {
        var btn = document.createElement("i");

        btn.setAttribute("class", "fa fa-close");
        btn.setAttribute("style", "color: #26B99A; position: absolute;");
        btn.setAttribute("onclick", "clearImg(" + index + ");");

        previewHolder.append("<div id='img-" + index + "' style='min-width: 100px; overflow: hidden; display: inline-block;'> " + btn.outerHTML + " <img src='{{url('/images/loading.gif')}}'/> </div>");
        
    }

    
    
    var newImage = document.getElementById('img-'+index).children[1];

    var originalImage = document.getElementById("uploadFile").files[index];

    //Is Used for validate a valid file.
    if (!filterType.test(originalImage.type)) {
        alert("Please select a valid image.");
        return;
    }

    fileReader.onload = function(event) {
    
    var image = new Image();

    image.onload = function() {
        var canvas = document.createElement("canvas");
        var context = canvas.getContext("2d");
        canvas.width = image.width / 4;
        canvas.height = image.height / 4;
        context.drawImage(image,
            0,
            0,
            image.width,
            image.height,
            0,
            0,
            canvas.width,
            canvas.height
        );
        
        upload(canvas, newImage);
        loadImageFile(total_files, index+1);
    }
    image.src = event.target.result;
  };

  fileReader.readAsDataURL(originalImage);
}

function upload(canvas, newImage) {
    var formData = new FormData();

    {
        var b64Image = canvas.toDataURL('image/jpeg');
        var u8Image = b64ToUint8Array(b64Image);
        formData.append('photos_id', $('#photos_id').val());
        formData.append("uploadFile", new Blob([u8Image], {
            type: "image/jpg"
        }));
    }

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        console.log(xhr.responseText);
        
        newImage.src = canvas.toDataURL();
    }
    
    xhr.open("POST", "/admin/manage/upload", true);
    xhr.send(formData);
}

function b64ToUint8Array(b64Image) {
    var img = atob(b64Image.split(',')[1]);
    var img_buffer = [];
    var i = 0;

    while (i < img.length) {
        img_buffer.push(img.charCodeAt(i));
        i++;
    }

    return new Uint8Array(img_buffer);
} 

function checkUpload(){
    /*alert(document.getElementsByClassName('image_preview')[{{empty($car->photos_id)?'0':'1'}}].childNodes.length);return false;
    if(document.getElementsByClassName('image_preview')[{{empty($car->photos_id)?'0':'1'}}].childNodes.length < 1){
        alert('Please attach images.');
        return false;
    }*/

    return true;
}
</script>