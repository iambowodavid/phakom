@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('admin.sidebar')

         <div class ="col-md-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

    
                              <div class ="dashboard-content-section" style ="height: auto; padding: 0px">

                                <div class ="bluetop-border user-header">

                                  
                                  <div class ="heading">User Details</div>

                                  <div class ="allback-users"><a class ="btn btn-primary" href ="{{url('admin/users')}}">Back</a></div>



                                </div>

                                <table class="table table-bordered table-striped">
                                      
                                  <tbody>
                                    <tr>
                                        <td class ="table-header">Username</td>
                                        <td>{{ $user->name}}</td>
                                    </tr>

                                    <tr>
                                      
                                      <td class ="table-header">Email Address</td>
                                      <td>{{ $user->email}}</td>
                                    </tr>
                                    
                                    <tr>
                                      <td class ="table-header">Phone Number</td>
                                      <td>{{ $user->phone_number}}</td>
                                    </tr>
                                    
                                  </tbody>
                                </table>

                                   
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    
</div>
@endsection

        


    


