<div class ="col-md-12">

    <div class ="dashboard-content-section" style ="height: auto; padding: 15px; margin: 0px 0px 30px 0px; display: flex; justify-content: space-between;">

        <div class ="heading" style="padding-top: 6px">All Lands</div>


        <!--<div><input type="text" class="form-control" id="search-users" onkeyup="searchUsers()" placeholder="Enter Username"></div>-->


    </div>

    <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
        <div class ="table-responsive">
            <table class="table" id ="users-table">

                <thead>
                    <tr>
                        <th>Owner</th>
                        <th>Title</th>
                        <th>Area Size</th>
                        <th>State</th>
                        <th>LGA</th>
                        <th>Date</th>
                        <th>Notice</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($lands as $land)
                    <tr @php echo $land->status === 'inactive' ? 'style="background: pink;"' : '' @endphp >

                        <td>{{ (\App\User::findOrNew($land->owned_by))->name}}</td>
                        <td>{{ $land->title }}</td>
                        <td>{{ $land->area_size }}</td>
                        <td>{{ $land->location_state }}</td>
                        <td>{{ $land->location_lga }}</td>
                        <td>{{ $land->created_at }}</td>
                        <td>{{ $land->notice }}</td>
                        
                        @php if(\Auth::user()->is_admin) { @endphp <td><a href = '{{ url("/admin/manage/land/refresh/listing/$land->id") }}' ><i class="fa fa-refresh text-success"></i></a></td> @php } @endphp
                        <td><a href = '{{ url("/admin/manage/land/$land->id") }}' ><i class="fa fa-pencil text-info"></i></a></td>
                        <td><a href = '#' onclick="if (confirm('Are you sure you want to delete {{ $land->name.' '.$land->name }}?')) {window.location = '{{ url("/admin/delete/land/$land->id") }}';} else {};"><i class="fa fa-trash text-danger"></i></a></td>
                        <td><a href = '{{ url("/admin/manage/land/set/notice/$land->id") }}' ><i class="fa fa-warning text-info"></i></a></td>
                        @php if(\Auth::user()->is_admin) { @endphp <td><a href = '{{ url("/admin/manage/land/toggle/status/$land->id") }}' ><i class="fa fa-info text-info"></i></a></td> @php } @endphp
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

	<a href='{{url("/admin/manage/land/-1")}}'>
        <button type="button" class="btn btn-danger btn-lg">List New Land</button>
    </a>
		
</div>