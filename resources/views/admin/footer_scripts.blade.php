<script src="/panel/js/bootstrap.min.js">
</script>

<!-- icheck -->
<script src="/panel/js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="/panel/js/moment.min2.js"></script>
<script type="text/javascript" src="/panel/js/datepicker/daterangepicker.js"></script>
<!-- Autocomplete -->
<script type="text/javascript" src="/panel/js/autocomplete/countries.js"></script>
<script src="/panel/js/autocomplete/jquery.autocomplete.js"></script>

<script src="/panel/js/custom.js"></script>