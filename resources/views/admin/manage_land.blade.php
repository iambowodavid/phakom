@include('admin.head_scripts')

<body class="nav-md">

    <div class="container body" style='background-color: white;'>

        <div class="main_container">

    @if (session('status'))
    <div class="alert alert-success" align="center">
        {{ session('status') }}
    </div>
    @endif
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3 style='color: #cc0000;'>Phakom</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Lands</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br/>

									@include('admin.multi_upload')

                                    <form enctype="multipart/form-data" method='POST' data-parsley-validate class="form-horizontal form-label-left" onsubmit='return checkUpload()>
                                        @csrf
                                        
                                        <input type='hidden' name='proposed-id' value='{{ $land->id }}'/>
                                        <input type='hidden' name='photos_id' id='photos_id' value='{{ \Carbon\Carbon::now()->timestamp }}'/>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post-title">Post Title <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name='title' type="text" id="post-title" required="required" class="form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('title')) ? old('title') : $land->title }}">

                                                    @if ($errors->has("title"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("title") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post-title">Description <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="textarea" required="required" name="description" class="form-control col-md-7 col-xs-12">{{ !empty(old('description')) ? old('description') : $land->description }}</textarea>
                                            </div>
                                        </div>
							
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="surface">Area size (sqkm) <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="surface" name="area_size" required="required" class="number form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('area_size')) ? old('area_size') : $land->area_size }}">

                                                    @if ($errors->has("area_size"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("area_size") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model">Standard price &#8358;<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="model" name="standard_price" required="required" class="number form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('standard_price')) ? old('standard_price') : $land->standard_price }}">

                                                    @if ($errors->has("standard_price"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("standard_price") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model">Distress price &#8358;<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="model" name="distress_price" class="number form-control col-md-7 col-xs-12"
                                                    value="{{ !empty(old('distress_price')) ? old('distress_price') : $land->distress_price }}">

                                                    @if ($errors->has("distress_price"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("distress_price") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Distress Ending Date <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input autocomplete="off" name='distress_ending_date' id="distress_ending_date" class="date-picker form-control col-md-7 col-xs-12" type="text"
                                                    value="{{ !empty(old('distress_ending_date')) ? old('distress_ending_date') : $land->distress_ending_date }}">

                                                    @if ($errors->has("distress_ending_date"))
                                                    <span class="invalid-feedback" style='color: red;'>
                                                       <strong>{{ str_replace("_", " ", $errors->first("distress_ending_date") ) }}</strong>
                                                    </span>
                                                    @endif
                                            </div>
                                        </div>

                                        <h2 align='center'>Location:</h2>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">State <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="location_state" id="state" class="form-control">
                                                    <option value="" selected="selected">- Select -</option>
                                                    <option value='Abia'>Abia</option>
                                                    <option value='Adamawa'>Adamawa</option>
                                                    <option value='AkwaIbom'>AkwaIbom</option>
                                                    <option value='Anambra'>Anambra</option>
                                                    <option value='Bauchi'>Bauchi</option>
                                                    <option value='Bayelsa'>Bayelsa</option>
                                                    <option value='Benue'>Benue</option>
                                                    <option value='Borno'>Borno</option>
                                                    <option value='Cross River'>Cross River</option>
                                                    <option value='Delta'>Delta</option>
                                                    <option value='Ebonyi'>Ebonyi</option>
                                                    <option value='Edo'>Edo</option>
                                                    <option value='Ekiti'>Ekiti</option>
                                                    <option value='Enugu'>Enugu</option>
                                                    <option value='FCT'>FCT</option>
                                                    <option value='Gombe'>Gombe</option>
                                                    <option value='Imo'>Imo</option>
                                                    <option value='Jigawa'>Jigawa</option>
                                                    <option value='Kaduna'>Kaduna</option>
                                                    <option value='Kano'>Kano</option>
                                                    <option value='Katsina'>Katsina</option>
                                                    <option value='Kebbi'>Kebbi</option>
                                                    <option value='Kogi'>Kogi</option>
                                                    <option value='Kwara'>Kwara</option>
                                                    <option value='Lagos'>Lagos</option>
                                                    <option value='Nasarawa'>Nasarawa</option>
                                                    <option value='Niger'>Niger</option>
                                                    <option value='Ogun'>Ogun</option>
                                                    <option value='Ondo'>Ondo</option>
                                                    <option value='Osun'>Osun</option>
                                                    <option value='Oyo'>Oyo</option>
                                                    <option value='Plateau'>Plateau</option>
                                                    <option value='Rivers'>Rivers</option>
                                                    <option value='Sokoto'>Sokoto</option>
                                                    <option value='Taraba'>Taraba</option>
                                                    <option value='Yobe'>Yobe</option>
                                                    <option value='Zamfara'>Zamafara</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">LGA <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select style='padding-top: 10px;' name="location_lga" id="lga" class="form-control" required></select>
                                            </div>
                                        </div>
										
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <a href='{{ url("admin") }}' class="btn btn-warning">Go Back</a>
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                <button type="submit" class="btn btn-success">Create Ad</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#distress_ending_date').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4",
								locale: {
									format: 'DD-MM-YYYY'
								}
                            }, function(start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>
                </div>

            </div>

        </div>
        <!-- /page content -->
    </div>

    </div>
    </div>

    <script>
       @php $location_state = $land->location_state; @endphp
       @php $location_lga = $land->location_lga; @endphp
       @include('admin.lga_js')
    </script>

    <script src='{{ url("/js/number-formatter.js") }}'></script>

	@include('admin.footer_scripts')
</body>

</html>