<div class ="col-md-12">

    <div class ="dashboard-content-section" style ="height: auto; padding: 15px; margin: 0px 0px 30px 0px; display: flex; justify-content: space-between;">

        <div class ="heading" style="padding-top: 6px">All Cars</div>


        <!--<div><input type="text" class="form-control" id="search-users" onkeyup="searchUsers()" placeholder="Enter Username"></div>-->


    </div>

    <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
        <div class ="table-responsive">
            <table class="table" id ="users-table">

                <thead>
                    <tr>
                        <th>Owner</th>
                        <th>Title</th>
                        <th>Brand</th>
                        <th>Model</th>
                        <th>Year</th>
                        <th>Standard Price</th>
                        <th>Distress Price</th>
                        <th>Date</th>
                        <th>Notice</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($cars as $car)
                    <tr @php echo $car->status === 'inactive' ? 'style="background: pink;"' : '' @endphp >
                        <td>@php $owner = \App\User::findOrNew($car->owned_by); @endphp <a href = 'users/{{ $owner->id }}' >{{ $owner->name}}</a></td>
                        <td>{{ $car->title }}</td>
                        <td>{{ $car->brand }}</td>
                        <td>{{ $car->model }}</td>
                        <td>{{ $car->year }}</td>
                        <td>&#8358;{{ $car->standard_price }}</td>
                        <td>&#8358;{{ $car->distress_price }}</td>
                        <td>{{ $car->created_at }}</td>
                        <td>{{ $car->notice }}</td>
                        
                        @php if(\Auth::user()->is_admin) { @endphp <td><a href = '{{ url("/admin/manage/car/refresh/listing/$car->id") }}' ><i class="fa fa-refresh text-success"></i></a></td> @php } @endphp
                        <td><a href = '{{ url("/admin/manage/car/$car->id") }}' ><i class="fa fa-pencil text-info"></i></a></td>
                        <td><a href = '#' onclick="if (confirm('Are you sure you want to delete {{ $car->name.' '.$car->name }}?')) {window.location = '{{ url("/admin/delete/car/$car->id") }}';} else {};"><i class="fa fa-trash text-danger"></i></a></td>
                        <td><a href = '{{ url("/admin/manage/car/set/notice/$car->id") }}' ><i class="fa fa-warning text-info"></i></a></td>
                        @php if(\Auth::user()->is_admin) { @endphp <td><a href = '{{ url("/admin/manage/car/toggle/status/$car->id") }}' ><i class="fa fa-info text-info"></i></a></td> @php } @endphp

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

	<a href='{{url("/admin/manage/car/-1")}}'>
        <button type="button" class="btn btn-danger btn-lg">List New Automobile</button>
    </a>
		
</div>