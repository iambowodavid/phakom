<div class ="col-md-12">

    <div class ="dashboard-content-section" style ="height: auto; padding: 15px; margin: 0px 0px 30px 0px; display: flex; justify-content: space-between;">

        <div class ="heading" style="padding-top: 6px">All Housings</div>


        <!--<div><input type="text" class="form-control" id="search-users" onkeyup="searchUsers()" placeholder="Enter Username"></div>-->


    </div>

    <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
        <div class ="table-responsive">
            <table class="table" id ="users-table">

                <thead>
                    <tr>
                        <th>Owner</th>
                        <th>Title</th>
                        <th>For</th>
                        <th>Surface Size</th>
                        <th>Bedrooms</th>
                        <th>State</th>
                        <th>LGA</th>
                        <th>Date</th>
                        <th>Notice</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($housings as $housing)
                    <tr @php echo $housing->status === 'inactive' ? 'style="background: pink;"' : '' @endphp >
                        <td>{{ (\App\User::findOrNew($housing->owned_by))->name}}</td>
                        <td>{{ $housing->title }}</td>
                        <td>{{ $housing->condition }}</td>
                        <td>{{ $housing->surface_size }}</td>
                        <td>{{ $housing->bedrooms }}</td>
                        <td>{{ $housing->location_state }}</td>
                        <td>{{ $housing->location_lga }}</td>
                        <td>{{ $housing->created_at }}</td>
                        <td>{{ $housing->notice }}</td>
                        
                        @php if(\Auth::user()->is_admin) { @endphp <td><a href = '{{ url("/admin/manage/housing/refresh/listing/$housing->id") }}' ><i class="fa fa-refresh text-success"></i></a></td> @php } @endphp
                        <td><a href = '{{ url("/admin/manage/housing/$housing->id") }}' ><i class="fa fa-pencil text-info"></i></a></td>
                        <td><a href = '#' onclick="if (confirm('Are you sure you want to delete {{ $housing->name.' '.$housing->name }}?')) {window.location = '{{ url("/admin/delete/housing/$housing->id") }}';} else {};"><i class="fa fa-trash text-danger"></i></a></td>
                        <td><a href = '{{ url("/admin/manage/housing/set/notice/$housing->id") }}' ><i class="fa fa-warning text-info"></i></a></td>
                        @php if(\Auth::user()->is_admin) { @endphp <td><a href = '{{ url("/admin/manage/housing/toggle/status/$housing->id") }}' ><i class="fa fa-info text-info"></i></a></td> @php } @endphp

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

	<a href='{{url("/admin/manage/housing/-1")}}'>
        <button type="button" class="btn btn-danger btn-lg">List New House</button>
    </a>
		
</div>