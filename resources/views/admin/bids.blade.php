@php
function getBidItemAndLink($bid){
    switch($bid->type){
        case 'car': return [ (\App\Car::find($bid->resource_id)), url("/car/$bid->resource_id")];
        case 'land': return [ (\App\Land::find($bid->resource_id)), url("/land/$bid->resource_id")];
        case 'housing': return [ (\App\Housing::find($bid->resource_id)), url("/housing/$bid->resource_id")];
    }
}
@endphp

@extends('layouts.app') @section('content')
<div class="container-fluid dashboard-container">

    @if (session('status'))
    <div class="alert alert-success" align="center">
        {{ session('status') }}
    </div>
    @endif

    <div class="row">

        @include('admin.sidebar')

        <div class="col-md-10 col-sm-10 col-xs-10 dashboard-body">

            <div class="col-md-12">

                <div class="dashboard-content-section" style="height: auto; padding: 15px; margin: 0px 0px 30px 0px; display: flex; justify-content: space-between;">

                    <div class="heading" style="padding-top: 6px">All Bids</div>

                    <!--<div><input type="text" class="form-control" id="search-users" onkeyup="searchUsers()" placeholder="Enter Username"></div>-->

                </div>

                <div class="dashboard-content-section" style="height: auto; padding: 0px">
                    <div class="table-responsive">
                        <table class="table" id="users-table">

                            <thead>
                                <tr>
                                    <th><h4>Bid Amount</h4></th>
                                    <th>Standard Price</th>
                                    <th>Distress Price</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>For</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bids as $bid)
                                @php
                                $itemAndLink = getBidItemAndLink($bid);
                                $item = $itemAndLink[0];
                                $link = $itemAndLink[1];
                                @endphp
                                <tr>
                                    <td>&#8358; {{ $bid->bid_amount }}</td>
                                    <td>&#8358; {{ $item->standard_price }}</td>
                                    <td>&#8358; {{ $item->distress_price }}</td>
                                    <td>{{ $bid->email }}</td>
                                    <td>{{ $bid->phone }}</td>
                                    <td>{{ $item->title . " " .$bid->type }}</td>
                                    <td><a href='{{ $link }}'>Visit LInk</td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>

@endsection