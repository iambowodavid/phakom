@extends('layouts.app') @section('content')
<div class="container-fluid dashboard-container">

    @if (session('status'))
    <div class="alert alert-success" align="center">
        {{ session('status') }}
    </div>
    @endif

    <div class="row">

        @include('admin.sidebar')

        <div class="col-md-10 col-sm-10 col-xs-10 dashboard-body">
        
            @include('admin.list_of_cars') <p style='padding-top: 35px;'></p>
            @include('admin.list_of_lands') <p style='padding-top: 35px;'></p>
            @include('admin.list_of_housings') <p style='padding-top: 35px;'></p>
        </div>

    </div>

</div>

@endsection