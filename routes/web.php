<?php

Auth::routes();

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('/setup', 'Setup@index');

Route::get('/', 'HomePage@index');
Route::get('/home', function(){ return redirect()->away('/admin/dashboard'); });
Route::get('/admin/', function(){ return redirect()->away('/admin/dashboard'); });
Route::get('/admin/dashboard', 'Admin\Dashboard@index');
Route::get('/admin/newsletter/manager', 'Admin\NewsletterManager@index');
Route::post('/admin/newsletter/manager', 'Admin\NewsletterManager@post');

//Cars
Route::get('/sell/car/{i}', 'Admin\ManageCar@new');
Route::get('/admin/manage/car/{id}', 'Admin\ManageCar@new');
Route::get('/admin/delete/car/{id}', 'Admin\ManageCar@delete');
Route::post('/admin/manage/car/{id}', 'Admin\ManageCar@updateOrCreateCar');
Route::get('/admin/manage/car/toggle/status/{id}', 'Admin\ManageCar@toggleStatus');
Route::get('/admin/manage/car/refresh/listing/{id}', 'Admin\ManageCar@refreshListing');
Route::get('/admin/manage/car/set/notice/{id}', 'Admin\ManageCar@setNotice');


//Lands
Route::get('/sell/land/{i}', 'Admin\ManageLand@new');
Route::get('/admin/manage/land/{id}', 'Admin\ManageLand@new');
Route::get('/admin/delete/land/{id}', 'Admin\ManageLand@delete');
Route::post('/admin/manage/land/{id}', 'Admin\ManageLand@updateOrCreateLand');
Route::get('/admin/manage/land/toggle/status/{id}', 'Admin\ManageLand@toggleStatus');
Route::get('/admin/manage/land/refresh/listing/{id}', 'Admin\ManageLand@refreshListing');
Route::get('/admin/manage/land/set/notice/{id}', 'Admin\ManageLand@setNotice');

//Housings
Route::get('/sell-rent/house/{i}', 'Admin\ManageHousing@new');
Route::get('/admin/manage/housing/{id}', 'Admin\ManageHousing@new');
Route::get('/admin/delete/housing/{id}', 'Admin\ManageHousing@delete');
Route::post('/admin/manage/housing/{id}', 'Admin\ManageHousing@updateOrCreateHousing');
Route::get('/admin/manage/housing/toggle/status/{id}', 'Admin\ManageHousing@toggleStatus');
Route::get('/admin/manage/housing/refresh/listing/{id}', 'Admin\ManageHousing@refreshListing');
Route::get('/admin/manage/housing/set/notice/{id}', 'Admin\ManageHousing@setNotice');

//other admin routes
Route::get('/admin/users/', 'AllUsersController@index');
Route::get('/admin/users/{id}', 'AllUsersController@individual_user');

//Bids
Route::get('/admin/bids', 'BidManager@show')->middleware("admin");

//image uploads
Route::post('/admin/manage/upload', 'Admin\ManageUploads@upload');

//MAIN SITE
Route::get("/about/us/", function(){ return redirect()->away('/admin/dashboard'); });
Route::post('/bid/{id}', 'BidManager@doBid');

Route::get('/sellforme', 'SellForMe@dindex');

Route::post('//subscribe-to-newsletter', 'SubscribeNewsletter@index');

Route::get('/cars', 'ShowLists@cars');
Route::get('/car/{id}', 'ShowItem@car');

Route::get('/lands', 'ShowLists@lands');
Route::get('/land/{id}', 'ShowItem@land');

Route::get('/housings', 'ShowLists@housings');
Route::get('/housing/{id}', 'ShowItem@housing');




//SOCIAL ROUTES
Route::get('/refresh-linkedin', '\App\SocialHelper@doLinkedInLogin');
Route::get('/signin-linkedin', '\App\SocialHelper@displayNewLinkedInCode');