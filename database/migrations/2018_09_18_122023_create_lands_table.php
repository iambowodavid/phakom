<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->timestamps();

            $table->string('status')->default('inactive');
            $table->string('photos_id')->default('');
            $table->string('notice')->default('');
            $table->string('owned_by')->default('');
            $table->string('title')->default('');
            $table->string('description')->default('');
            $table->string('area_size')->default('');
            $table->string('standard_price')->default('');
            $table->string('distress_price')->default('');
            $table->string('distress_ending_date')->default('');
            $table->string('location_state')->default('');
            $table->string('location_lga')->default('');
            $table->string('category')->default('');
            $table->integer('views')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
}
