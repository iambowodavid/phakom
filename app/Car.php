<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Car extends Authenticatable
{
    protected $fillable = [
        'photos_id',
        'status',
        'notice',
        'owned_by',
        'title',
        'description',
        'brand',
        'model',
        'year',
        'car_type',
        'condition',
        'mileage',
        'transmission',
        'car_registered',
        'standard_price',
        'distress_price',
        'distress_ending_date',
        'location_state',
        'location_lga',
        'category'
    ];
}
