<?php

namespace App\Http\Controllers;

use DB;
use App\Bid;
use App\Car;
use App\Land;
use App\Housing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BidManager extends Controller
{
  public function show(){
    return view('admin.bids', ['bids' => Bid::all()]);
  }

  public function doBid($id, Request $request)
  {
    $validation = Validator::make($request->all(),
    [
      'email' => "required|string",
      'phone' => "required|numeric",
      'bid_amount' => "required|string",
    ]);

    if($validation->fails())
    {
        return back()->withErrors($validation);
    }

    Bid::create($request->all() + ["resource_id" => $id]);

    return back()->with('status', 'success');
  }
}