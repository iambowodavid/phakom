<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Car;
use App\MailMan;
use App\SocialHelper;
use Illuminate\Http\Request;
use App\NewsletterSubscriber;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\ManageUploads;

class NewsLetterManager extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.newsletter_manager');
    }

    public function post(Request $request)
    {
      $title = $request->get("title");
      $message = $request->get("message");

      $correspondence = NewsletterSubscriber::all();

      foreach($correspondence as $subscriber){
         MailMan::sendNewsletter($subscriber->email, $subscriber->name, $title, $message);
      }

      return redirect()->back()->with("status", "success");
    }
}