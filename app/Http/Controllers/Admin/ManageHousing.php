<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Housing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ManageHousing extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');
    }

    public function new($id)
    {
        $housing = Housing::findOrNew($id);

        return view('admin.manage_housing', ['housing'=>$housing]);
    }

    public function updateOrCreateHousing(Request $request)
    {
        $this->validate($request,
        [
            'title' => "required|string",
        ]);

        $raw = $request->all();

        $raw['owned_by'] = Auth::user()->id;

        foreach($raw as $key => $value)
        {
            if($value == null)
            {
                $raw[$key] = "";
            }
        }

        $housing = Housing::find($request->input('proposed-id'));

        if($housing != null)
        {
            $housing->update($raw);
        }
        else
        {
            $raw['status'] = Auth::user()->is_admin ? 'active' : 'inactive';
    
            $housing = Housing::create($raw);
        }

        $message = $housing->title .'.           '. $housing->description;
        $photos = ManageUploads::getFilesURLs($housing->photos_id);
        $url = config("app.url").("/items/housings/$housing->id");

        //SocialHelper::postPhotosOnFacebook($photos, $message, $url);
        //SocialHelper::postPhotosToLinkedIn($photos, $message, $url);
        //SocialHelper::postPhotoToTwitter($photos, $message, $url);

        return redirect('/admin/dashboard')->with('status', 'Data saved successfully.');
    }

    public function refreshListing($id)
    {
        $housing = Housing::where('id', $id);

        $housing->update(['created_at' => \Carbon\Carbon::now()]);

	    return redirect('/admin/dashboard')->with('status', 'Listing refreshed successfully.');
    }

    public function setNotice($id, Request $request)
    {
        $housing = Housing::where('id', $id)->first();

        //$noticeText = $request->input('notice');

        //$noticeText = empty($noticeText) ? '' : $noticeText;
        $noticeText = $housing->notice !== 'sold/unavailabe' ? 'sold/unavailabe' : '';

        $housing->update(['notice' => $noticeText]);

	    return redirect('/admin/dashboard')->with('status', 'Listing notice set successfully.');
    }

    public function toggleStatus($id)
    {
        $housing = Housing::where('id', $id)->first();

        $newStatus = $housing->status === 'inactive' ? 'active' : 'inactive';

        $housing->update(['status' => $newStatus]);

	    return redirect('/admin/dashboard')->with('status', "Listing status successfully changed to $newStatus.");
    }

    public function delete($id)
    {
        DB::delete('delete from housings where id = ?',[$id]);

	    return redirect('/admin/dashboard')->with('status', 'Data deleted successfully.');
    }
}