<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Car;
use App\SocialHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\ManageUploads;

class ManageCar extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');
    }

    public function new($id)
    {
        $car = Car::findOrNew($id);

        return view('admin.manage_car', ['car'=>$car]);
    }

    public function updateOrCreateCar(Request $request)
    {
        $this->validate($request,
        [
            'title' => "required|string",
        ]);

        $raw = $request->all();

        $raw['owned_by'] = Auth::check() ? Auth::user()->id : -1;

        foreach($raw as $key => $value)
        {
            if($value == null)
            {
                $raw[$key] = "";
            }
        }

        $car = Car::find($request->input('proposed-id'));

        if($car != null)
        {
            $car->update($raw);
        }
        else
        {
            $raw['status'] = Auth::check() && Auth::user()->is_admin ? 'active' : 'active';//'inactive';
    
            $car = Car::create($raw);
        }

        $message = $car->title .'.           '. $car->description;
        
        $photos = array();
        if(is_dir(public_path("/photos/$car->photos_id"))){
            $photos = ManageUploads::getFilesURLs($car->photos_id);
        }

        $url = config("app.url").("/car/$car->id");

        if(count($photos) > 0)
        {
            //SocialHelper::postPhotosOnFacebook($photos, $message .".\r\n \r\n Buy Now At: ". $url, $url);
            SocialHelper::postPhotoToLinkedIn($photos, $message, $url);
            SocialHelper::postPhotoToTwitter($photos, $message .".\r\n \r\n Buy Now At: ". $url . " \r\n \r\n", $url);
            //die("posting social");
        }

        return redirect('/admin/dashboard')->with('status', 'Data saved successfully.');
    }

    public function refreshListing($id)
    {
        $car = Car::where('id', $id);

        $car->update(['created_at' => \Carbon\Carbon::now()]);

	    return redirect('/admin/dashboard')->with('status', 'Listing refreshed successfully.');
    }

    public function setNotice($id, Request $request)
    {
        $car = Car::where('id', $id)->first();

        //$noticeText = $request->input('notice');

        //$noticeText = empty($noticeText) ? '' : $noticeText;
        $noticeText = $car->notice !== 'sold/unavailabe' ? 'sold/unavailabe' : '';

        $car->update(['notice' => $noticeText]);

	    return redirect('/admin/dashboard')->with('status', 'Listing notice set successfully.');
    }

    public function toggleStatus($id)
    {
        $car = Car::where('id', $id)->first();

        $newStatus = $car->status === 'inactive' ? 'active' : 'inactive';

        $car->update(['status' => $newStatus]);

	    return redirect('/admin/dashboard')->with('status', "Listing status successfully changed to $newStatus.");
    }

    public function delete($id)
    {
        DB::delete('delete from cars where id = ?',[$id]);

	    return redirect('/admin/dashboard')->with('status', 'Data deleted successfully.');
    }
}