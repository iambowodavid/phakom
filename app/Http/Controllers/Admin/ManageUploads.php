<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Car;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ManageUploads extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');
    }

    public function upload(Request $request)
    {
        $input = $request->all();
        
        if($file=$request->file('uploadFile'))
        {
            {
               $folderName = $request->input('photos_id');

               $fileName = mt_rand().'.jpg';

               $file->move(public_path("/photos/$folderName"), $fileName);
            }
       }

       return 'success';
    }
    
    public static function getFiles($id)
    {
        return array_values( array_diff(scandir(public_path("/photos/$id")), array('..', '.')) );
    }

    public static function getFilesURLs($id)
    {
        $fileIDs = self::getFiles($id);

        $urls = array();

        foreach($fileIDs as $fileID)
        {
            array_push($urls, config("app.url")."/photos/$id/$fileID");
        }

        return $urls;
    }
}