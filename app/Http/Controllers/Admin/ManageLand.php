<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Land;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ManageLand extends Controller
{ 
    public function __construct()
    {
		$this->middleware('auth');
    }

    public function new($id)
    {
        $land = Land::findOrNew($id);

        return view('admin.manage_land', ['land'=>$land]);
    }

    public function updateOrCreateLand(Request $request)
    {
        $this->validate($request,
        [
            'title' => "required|string",
        ]);

        $raw = $request->all();

        $raw['owned_by'] = Auth::user()->id;

        foreach($raw as $key => $value)
        {
            if($value == null)
            {
                $raw[$key] = "";
            }
        }

        $land = Land::find($request->input('proposed-id'));

        if($land != null)
        {
            $land->update($raw);
        }
        else
        {
            $raw['status'] = Auth::user()->is_admin ? 'active' : 'inactive';
    
            $land = Land::create($raw);
        }

        $message = $land->title .'.           '. $land->description;
        $photos = ManageUploads::getFilesURLs($land->photos_id);
        $url = config("app.url").("/items/lands/$land->id");

        //SocialHelper::postPhotosOnFacebook($photos, $message, $url);
        //SocialHelper::postPhotosToLinkedIn($photos, $message, $url);
        //SocialHelper::postPhotoToTwitter($photos, $message, $url);

        return redirect('/admin/dashboard')->with('status', 'Data saved successfully.');
    }

    public function refreshListing($id)
    {
        $land = Land::where('id', $id);

        $land->update(['created_at' => \Carbon\Carbon::now()]);

	    return redirect('/admin/dashboard')->with('status', 'Listing refreshed successfully.');
    }

    public function setNotice($id, Request $request)
    {
        $land = Land::where('id', $id)->first();

        //$noticeText = $request->input('notice');

        //$noticeText = empty($noticeText) ? '' : $noticeText;
        $noticeText = $land->notice !== 'sold/unavailabe' ? 'sold/unavailabe' : '';

        $land->update(['notice' => $noticeText]);

	    return redirect('/admin/dashboard')->with('status', 'Listing notice set successfully.');
    }

    public function toggleStatus($id)
    {
        $land = Land::where('id', $id)->first();

        $newStatus = $land->status === 'inactive' ? 'active' : 'inactive';

        $land->update(['status' => $newStatus]);

	    return redirect('/admin/dashboard')->with('status', "Listing status successfully changed to $newStatus.");
    }

    public function delete($id)
    {
        DB::delete('delete from lands where id = ?',[$id]);

	    return redirect('/admin/dashboard')->with('status', 'Data deleted successfully.');
    }
}