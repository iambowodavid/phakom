<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Bid;
use App\Car;
use App\Land;
use App\Housing;

class Dashboard extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index()
    {
         $user = \Auth::user();

         $id = $user->is_admin ? '%' : $user->id;

	    	 {
            $cars = Car::where('owned_by','LIKE',$id)->get()->sortByDesc('created_at');
            $lands = Land::where('owned_by','LIKE',$id)->get()->sortByDesc('created_at');
            $housings = Housing::where('owned_by','LIKE',$id)->get()->sortByDesc('created_at');
			
			      return view('admin.dashboard', ['bids' => Bid::all(), 'cars'=>$cars, 'lands'=>$lands, 'housings'=>$housings]);
		    }
  }
}
