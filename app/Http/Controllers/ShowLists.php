<?php

namespace App\Http\Controllers;

use DB;
use App\Car;
use App\Land;
use App\Housing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowLists extends Controller
{
    public function cars(Request $request)
    {
        $cars = $this->filter($request, Car::class)->sortByDesc('created_at');

		return view('cars', ['cars'=>$cars]);
    }

    public function lands(Request $request)
    {
        $lands = $this->filter($request, Land::class)->sortByDesc('created_at');

		return view('lands', ['lands'=>$lands]);
    }

    public function housings(Request $request)
    {
        $housings = $this->filter($request, Housing::class)->sortByDesc('created_at');

		return view('housings', ['housings'=>$housings]);
    }

    private function filter(Request $request, $type)
    {
        $isSearch = !empty($request->input('dosearch'));
        $category = $request->input('category');
        $keyword = $request->input('keyword');
        $location = $request->input('location');
        $price_range = $request->input('price_range');

        if($isSearch){
            $items = $type::where('title', 'LIKE', '%'.$keyword.'%')
            ->where('location_lga', 'LIKE', '%'.$location.'%');
            if(!empty($category)){$items = $items->where('category', $category);}
            $items = $items->get();

            if(!empty($price_range)){
                foreach($items as $key => $item){
                    $price = NULL;
    
                    if(!empty($item->distress_price)){
                        $price = str_replace(',', '', $item->distress_price);
                    }
                    else{
                        $price = str_replace(',', '', $item->standard_price);
                    }
                    if(!($price >= explode(',', $price_range)[0] && $price <= explode(',', $price_range)[1]))
                    {
                        $items->forget($key);
                    }
                }
            }
        }
        else{
            $items = $type::all();
        }

        return $items;
    }
}
