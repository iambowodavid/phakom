<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class HomePage extends Controller
{
    public function index(Request $request)
    {
      return view('home_page', ['request'=>$request]);
	  }
}