<?php

namespace App\Http\Controllers;

use DB;
use App\Bid;
use App\Car;
use App\Land;
use App\Housing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\NewsletterSubscriber;

class SubscribeNewsletter extends Controller
{
  public function index(Request $request)
  {
    $validation = Validator::make($request->all(),
    [
      'email' => "required|email",
      'name' => "required|string",
    ]);

    if($validation->fails())
    {
      return response()->json($validation->errors());
    }
    else
    {
      NewsletterSubscriber::create($request->all());

      return back()->with('status', 'success')->withCookie(cookie('subscribed-to-newsletter', 'true', 365 * 24 * 60 * 60));;
    }
  }
}