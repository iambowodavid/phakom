<?php

namespace App\Http\Controllers;

use DB;
use App\Car;
use App\Land;
use App\Housing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;

class ShowItem extends Controller
{
    public function car($id)
    {
	    	 {
            $car = Car::find($id);

            if($car == null){
              return 'wrong link.';
            }

			      return view('car_item', ['car'=>$car]);
		    }
  }

  public function land($id)
  {
       {
          $land = Land::find($id);

          if($land == null){
            return 'wrong link.';
          }

          //$this->checkViews('land', $car);

          return view('land_item', ['land'=>$land]);
      }
  }

  public function housing($id)
  {
       {
          $housing = Housing::find($id);

          if($housing == null){
            return 'wrong link.';
          }

          //$this->checkViews('housing', $car);

          return view('housing_item', ['housing'=>$housing]);
      }
  }

  public static function checkViews($prefix, $object)
  {
    $cookieName = $prefix . '_' . $object->id;

    $isExisting = !empty( Cookie::get($cookieName) );

    if($isExisting){
      
    }
    else
    {
      $object->increment('views');
      Cookie::queue( Cookie::forever($cookieName, $cookieName) );
    }
  }
}