<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Investment;
use DB;

class AllUsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
        $users = User::where("id", "!=", "1")->get();
       
        return view('admin.all_users', ['users'=>$users, 'bids'=>\App\Bid::all()]);
    }

    public function individual_user($id)
	  {
		     $user = User::find($id);

        return view('admin.individual_users',['user'=>$user, 'bids'=>\App\Bid::all()]);
    }


}
