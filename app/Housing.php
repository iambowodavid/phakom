<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Housing extends Authenticatable
{
    protected $fillable = [
        'photos_id',
        'status',
        'notice',
        'owned_by',
        'condition',
        'title',
        'description',
        'surface_size',
        'bedrooms',
        'bathrooms',
        'broker_free',
        'furnished',
        'standard_price',
        'distress_price',
        'distress_ending_date',
        'location_state',
        'location_lga',
        'category'
    ];
}
