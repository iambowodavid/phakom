<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Land extends Authenticatable
{
    protected $fillable = [
        'photos_id',
        'status',
        'notice',
        'owned_by',
        'title',
        'description',
        'area_size',
        'standard_price',
        'distress_price',
        'distress_ending_date',
        'location_state',
        'location_lga',
        'category'
    ];
}
