<?php

namespace App;

use App\Http\Controllers\Controller;

require_once(base_path() . '/CUSTOM/Facebook/autoload.php');
use Facebook\Facebook;
define('FACEBOOK_APP_ID', '530702997400242');
define('FACEBOOK_APP_SECRETE', '2603e5f65b6c72832ba9d4903fd8ee98');
define('FACEBOOK_PAGE_ACCESS_TOKEN', 'EAAHiqZBukKrIBANLYk13cXu8rZAeURnNDZCGuIHzDqEm005yUCv51i0ScMmwhQ39wEzBkoMBayWXY5GEN0ZCVz5GwVzZAxKQxbzCunYafoQHPvZBgP4hcWOx15BFXV8CpnvXitlbaDHEDLJc1V8kezWDDFU78Crpw8b9fuOFpCNgZDZD');

define('LINKED_IN_APP_ID', '77i3oxfcqltbv7');
define('LINKED_IN_APP_SECRETE', 'rvoOaE3TfnSu1B9I');
define('LINKED_IN_PAGE_ACCESS_TOKEN', 'AQVlqromIYdJpaWdhW8o-jTHbai-mcLOwHQv2NDn7fdDpRWweIqYr2o3aaC0NKoq52rXY6ukVjLeOA0olgbVXKmDFnsw1vy-6jotJinaiFAT5LikDK5iBdmxL3UWbaUhMG9-tnf6qZ1495_ZupUyHv9PTQOGf6zUbiuQvRAoQ1W3rFDN20mqt40B9rZ9sYv0sUwjz0qsMKMNAX_84EWqoeGSPVnC1dw2_V9b1t5vFPt9gb-cYg57ikhFMozDcoBj0_aruCC2e-A3b_wAqMva5HKuuxSmdK2x9RvQFaCY30uaT6stlRux6-waBHe-tqqAUGpSg7N9A1ie2EfgiJowG9vfZIJB3w');
define('LINKED_IN_REDIRECT_URL', config('app.url').('/signin-linkedin') );

require_once(base_path() . '/CUSTOM/Twitter/TwitterAPIExchange.php');
use TwitterAPIExchange;
const TWITTER_SETTINGS = array(
'consumer_key' => 'Jx1iYayPxOHMTzFlmHVDUI0Rx',
'consumer_secret' => 'Yv8GVBQVVjIwmwTS3PEyBz0E6mVm09pnP2ab1YSHk6uJFCiK5w',
'oauth_access_token' => '226839617-DlNB7XJthWQkDXEuTAmcFbZEMAgQKitOt63J8rif',
'oauth_access_token_secret' => 'bhavfDWcRoAANyyn9lw1DwWGz3IbLoE9wkuFyRqhp6xXk'
);

class SocialHelper extends Controller
{
    public static function postTextOnFacebook($message, $link)
    {
        $fb = new Facebook([
            'app_id' => FACEBOOK_APP_ID,
            'app_secret' => FACEBOOK_APP_SECRETE,
            'default_graph_version' => 'v2.2',
           ]);

           $pageAccessToken = FACEBOOK_PAGE_ACCESS_TOKEN;
        
           $linkData = [
            'link' => $link,
            'message' => $message
           ];

           $response = $fb->post('/me/feed', $linkData, $pageAccessToken);
           
           $graphNode = $response->getGraphNode();
    }

    public static function postPhotosOnFacebook($photos, $message, $link)
    {
        $fb = new Facebook([
            'app_id' => FACEBOOK_APP_ID,
            'app_secret' => FACEBOOK_APP_SECRETE,
            'default_graph_version' => 'v2.2',
           ]);

           $pageAccessToken = FACEBOOK_PAGE_ACCESS_TOKEN;
        
           $photoIDs = [];

           foreach($photos as $photo){
               $linkData = ['url' => $photo, 'published' => 'false'];
               
               array_push($photoIDs, $fb->post('/me/photos', $linkData, $pageAccessToken)->getGraphObject()->getProperty('id'));
           }
           
           $linkData = [
            'message' => $message
           ];
           
           for($i = 0; $i<count($photoIDs); $i++)
           {
               $linkData["attached_media[$i]"] = '{"media_fbid":"'.$photoIDs[$i].'"}';
           }

           $response = $fb->post('/me/feed', $linkData, $pageAccessToken);
           
           $graphNode = $response->getGraphNode();
    }
    
    public function doLinkedInLogin()
    {
        header('location:' . "https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=".LINKED_IN_APP_ID."&redirect_uri=".LINKED_IN_REDIRECT_URL."&state=987654321&scope=r_basicprofile r_emailaddress rw_company_admin w_share");
        die();
    }

    public function displayNewLinkedInCode()
    {
        if (isset($_GET['code']) && !empty($_GET['code']))
        {
            $code = $_GET['code'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://www.linkedin.com/oauth/v2/accessToken");
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS,"grant_type=authorization_code&code=".$code."&redirect_uri=".LINKED_IN_REDIRECT_URL."&client_id=".LINKED_IN_APP_ID."&client_secret=".LINKED_IN_APP_SECRETE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = json_decode( curl_exec ($ch) );
            curl_close ($ch);
        }
        
        die($server_output->access_token);
   }

   public static function postTextToLinkedIn($message, $link)
   {
       $endpoint = "https://api.linkedin.com/v1/people/~/shares?oauth2_access_token=".LINKED_IN_PAGE_ACCESS_TOKEN."&format=json";
        
        $data_string = ' 
        {
            "comment": "'.$message.' '.$link.'",
            "visibility": {
              "code": "anyone"
            }
          }
        ';
        
        $ch = curl_init($endpoint);
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-li-format: json',
            'Content-Length: ' . strlen($data_string),
        ));
        
        $result = curl_exec($ch);
        
        //closing
        curl_close($ch);
        
        return $result;
   }

   public static function postPhotoToLinkedIn($photos, $message, $link)
   {
       $endpoint = "https://api.linkedin.com/v1/people/~/shares?oauth2_access_token=".LINKED_IN_PAGE_ACCESS_TOKEN."&format=json";
        
        $data_string = json_encode(
            array (
            'comment' => $message,
            'content' => 
            array (
              'submitted-url' => $link,
              'submitted-image-url' => count($photos)>0 ?$photos[0] : '',
            ),
            'visibility' => 
            array (
              'code' => 'anyone',
            ),
          )
        )
        ;
        
        $ch = curl_init($endpoint);
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-li-format: json',
            'Content-Length: ' . strlen($data_string),
        ));
        
        $result = curl_exec($ch);
        
        //closing
        curl_close($ch);
        
        return ($result);
   }
   
   public static function postPhotoToTwitter($photos, $message, $link)
   {
       $mediaID = NULL;
       
       {
            $url = "https://upload.twitter.com/1.1/media/upload.json";
       
            $requestMethod = "POST";
       
            $postField = ['media_data' => base64_encode(file_get_contents($photos[0]))];
       
            $twitter = new TwitterAPIExchange(TWITTER_SETTINGS);
            
            $rawResponse = $twitter->setPostfields($postField)
                 ->buildOauth($url, $requestMethod)
                 ->performRequest();

            $results = json_decode($rawResponse);
            
            $mediaID = $results->media_id;
       }



       $url = "https://api.twitter.com/1.1/statuses/update.json";
    
       $requestMethod = "POST";
    
       $postField = ['status' => ($message . ' ' .$link), 'media_ids' => $mediaID];
    
       $twitter = new TwitterAPIExchange(TWITTER_SETTINGS);
    
       return $twitter->setPostfields($postField)
                 ->buildOauth($url, $requestMethod)
                 ->performRequest();
   }
}