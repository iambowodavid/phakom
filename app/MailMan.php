<?php

namespace App;

class MailMan
{
    public static function sendNewsletter($to, $name, $title, $message)
    {
        $HTML = file_get_contents(public_path().'/emails/newsletter.html');
        $HTML = str_replace(
            ['{{URL}}', '{{NAME}}', '{{TITLE}}', '{{MESSAGE}}'],
            [config('app.url'), $name, $title, $message],

            $HTML
        );

        $HTML = wordwrap($HTML,70);
      
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: <no-reply@>".config('app.url') . "\r\n";
        //$headers .= 'Cc: myboss@example.com' . "\r\n";

        mail($to, $title, $HTML, $headers);
    }
}