<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $fillable = [
        "resource_id",
        "bid_amount",
        "type",
        "email",
        "phone"
    ];
}